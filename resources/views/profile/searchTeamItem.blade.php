@if(count($results)>0)
    @foreach($results as $result)


        <div class="col-12 d2s-team-members">
            <div class="row">
                <div class="col-sm-2 text-center text-sm-left">
                    <img src="/images/teamlogos/{{$result->image}}"
                         class="rounded-circle img-fluid ">
                </div>
                <div class="col-12 col-sm-5 col-md-5 text-center text-sm-left">
                    <h5>{{$result->name}}</h5>
                    <p class=" small">{{$result->num_members." ".  str_plural('Member', $result->num_members)}}</p>
                    <p class="small">Team Bio</p>
                    <p class="small">{{$result->bio}}</p>
                </div>

                <div class="col-12 col-sm-12 col-md-5 text-center " id="team-actions">

                    @if(!$result->num_members <=5 && $result->status =="available")
                        @if(Auth::user()->profile->request_status == 'free')
                            <button class="btn btn-success size-2 float-none float-sm-none float-md-right text-center"
                                    id="joinTeam"
                                    onclick="join(this)"
                                    data-team-id="{{$result->id}}">Join
                            </button>

                        @else
                            <button class="btn btn-default size-2 float-none float-sm-none float-md-right text-center disabled" id="pending" onclick=""
                                    data-team-id="{{$result->id}}">Pending
                            </button>

                        @endif

                    @else
                        <button class="btn btn-default size-2 float-sm-none float-md-right text-center disabled" id="pending" onclick=""
                                data-team-id="{{$result->id}}">Team Full
                        </button>
                    @endif


                </div>
            </div>
            <small class="text-danger" id="team-error"></small>
        </div>
    @endforeach
@else
    <p><i class="fa fa-frown-o"></i>No results found</p>
@endif
