<div class="col-sm-12 d2s-team-members" id="member{{$userProfileToInvite->id}}">
    <div class="row">
        <div class="col-sm-2 text-center text-sm-left">
            <img src="/images/users/{{$userProfileToInvite->image}}"
                 class="rounded-circle img-fluid ">
        </div>
        <div class="col-sm-7 text-center text-sm-left">
            <h5>{{$userProfileToInvite->fullname}}</h5>
            <p>
                <small>{{$userProfileToInvite->email}}</small>
            </p>
            @if($userProfileToInvite->team_role =="")
                <small><a href="/edit#team-position"><i
                                class="fa fa-bullhorn"></i>
                        Request team role</a></small>
            @else
                <p>{{$userProfileToInvite->team_role}}</p>
            @endif

        </div>
        <div class="col-sm-3 text-center">

            <div class="dropdown">
                <a class="dropdown-toggle"
                   href="#" id="dropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    Actions
                </a>

                <div class="dropdown-menu text-center"
                     aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-item"  onclick="removeMember(this)" data-member-id="{{$userProfileToInvite->id}}">Remove
                        member</div>

                </div>
            </div>

        </div>
    </div>

</div>