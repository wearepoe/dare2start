@extends('userlayouts.master')


@section('extraStyles')

@stop

@section('content')

    @if($team==[])
        {{-- create team modal here --}}
        <div class="modal fade" id="createTeamModal" tabindex="-1" role="dialog" aria-labelledby="createTeamModal"
             aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div>
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title" id="createTeamModalLabel">Create a
                                team</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="color:white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form id="create-team-form" class="next-next">
                                <div>
                                    <div class="form-group fg_icon focus-2">
                                        <label for="fullname" class="form-label">Team Name</label>
                                        <input class="form-input" type="text" name="team-name" id="team-name"
                                               placeholder="Team Dare2Start">
                                    </div>

                                    <div class="form-group focus-2">
                                        <label class="form-label" for="industry">Industry</label>
                                        <select class="form-input" type="text" name="industry" id="industry">
                                            <option value="Advertising / Public Relations">Advertising / Public
                                                Relations
                                            </option>
                                            <option value="Agriculture">Agriculture</option>
                                            <option value="Architecture Services">Architecture Services</option>
                                            <option value="Banking and Finance">Banking and Finance</option>
                                            <option value="Construction">Construction</option>
                                            <option value="Education">Education</option>
                                            <option value="Fashion Design">Fashion Design</option>
                                            <option value="Food and Beverage">Food and Beverage</option>
                                            <option value="Health and Hospitality">Health and Hospitality</option>
                                            <option value="IT">IT</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Marketing and Sales">Marketing and Sales</option>
                                            <option value="Tourism">Tourism</option>
                                            <option value="Transport">Transport</option>
                                            <option value="Waste Management">Waste Management</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>

                                    <div class="form-group focus-2">
                                        <label class="form-label" for="level">Team Bio</label>
                                        <div class="form-group focus-2">
                                        <textarea id="team-bio" class="form-input"
                                                  placeholder="Something about the team"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>


                        <div class="modal-footer">
                            <button class="btn btn-primary" style="margin-right: 0" id="create-team-submit">
                                Create Team
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        </div>



        {{--join team modal--}}
        <div class="modal fade" id="joinTeamModal" tabindex="-1" role="dialog" aria-labelledby="joinTeamModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div>
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title" id="createTeamModalLabel">Search for a team</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="color:white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form id="create-team-form" class="search">
                                <div>
                                    <div class=" form-group input-group fg_icon focus-2">
                                        <input class=" form-control form-input" type="text" name="search-team"
                                               id="search-team"
                                               placeholder="eg: Team Aceup">

                                        <button class="btn btn-primary input-group-btn text-center"
                                                style="margin-right: 0; border-radius: 0 4px 4px 0 ; padding-top: 15px; font-size: 15px!important; "
                                                id="search-team-submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>

                            <div class="search-results" style="margin-top: 30px" id="search-team-results">


                            </div>
                        </div>


                        <div class="modal-footer">
                        </div>

                    </div>

                </div>
            </div>
        </div>

    @else
        <div class="modal fade" id="searchTeamModal" tabindex="-1" role="dialog" aria-labelledby="searchTeamModal"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div>
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title" id="createTeamModalLabel">Search for a team Member</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                    style="color:white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form id="create-team-form" class="search">
                                <div>
                                    <div class=" form-group input-group fg_icon focus-2">
                                        <input class=" form-control form-input" type="text" name="search"
                                               id="search"
                                               placeholder="eg: Kofi Nyarko">

                                        <button class="btn btn-primary input-group-btn text-center"
                                                style="margin-right: 0; border-radius: 0 4px 4px 0 ; padding-top: 15px; font-size: 15px!important; "
                                                id="search-submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>

                            <div class="search-results" style="margin-top: 30px" id="search-results">


                            </div>
                        </div>


                        <div class="modal-footer">
                        </div>

                    </div>

                </div>
            </div>
        </div>
    @endif

    <div class="container d2s-detail-container">
        <div class="row">
            <div class="col-12 col-md-4 left-feild">
                <div class="d2s-user-block style-3">
                    <div class="d2s-user-detail">
                        <a class="d2s-item-user style-2" href="/profile/edit">
                            <img src="/images/users/{{$user_info->image}}" alt="" class="img-fluid rounded-circle">
                        </a>
                        <a class="d2s-item-left btn btn-primary size-2 hover-1" href="/profile/edit"><i
                                    class="fa fa-edit"></i>Edit</a>
                        <div class="d2s-item-right btn btn-primary btn-share size-2 hover-7">
                            <i class="fa fa-share-alt"></i>share
                        </div>
                        <p class="d2s-use-name">{{$user_info->fullname}}</p>
                        <div class="d2s-user-info">
                            {{$user_info->email}}
                        </div>

                        @if(!$team==[])
                            <div class="d2s-text-tags style-2">

                                @if($user_info->team_role =="")
                                    <small><a href="/profile/edit#team-role"><i class="fa fa-plus"></i>
                                            Add
                                            your role in the team</a></small>
                                @else
                                    <p>{{$user_info->team_role}}</p>
                                @endif

                            </div>
                        @endif


                        <div class="d2s-user-social">
                            @if(!$user_info->facebook == "")
                                <a class="social-btn color-1"
                                   href="https://www.facebook.com/{{$user_info->facebook}}" target="_blank"><i
                                            class="fa fa-facebook"></i></a>
                            @endif
                            @if(!$user_info->twitter == "")
                                <a class="social-btn color-2" href="https://www.twitter.com/{{$user_info->twitter}}"
                                   target="_blank"><i
                                            class="fa fa-twitter"></i></a>
                            @endif
                            @if(!$user_info->instagram == "")
                                <a class="social-btn" style=" background-color: #fb3958; color:#fff;"
                                   href="https://www.instagram.com/{{$user_info->instagram}}" target="_blank"><i
                                            class="fa fa-instagram"></i></a>
                            @endif
                            @if(!$user_info->linkedin == "")
                                <a class="social-btn color-6"
                                   href="https://www.linkedin.com/in/{{$user_info->linkedin}}" target="_blank"><i
                                            class="fa fa-linkedin"></i></a>
                            @endif

                            @if($user_info->facebook == "" && $user_info->twitter == "" && $user_info->instagram == "" && $user_info->linkedin == "")
                                <a href="/profile/edit#social-media-handles">
                                    <p><i class="fa fa-plus"></i> Add social media handles</p>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="d2s-desc-block">
                    <div class="d2s-desc-author">
                        <div class="d2s-desc-label text-center text-md-left">About Me</div>
                        <div class="clearfix"></div>
                        <div class="d2s-desc-text  text-center text-md-left">
                            @if(empty($user_info->bio))
                                <a href="/profile/edit#about-me"><i class="fa fa-plus"></i> Add a brief bio</a>
                            @else
                                {{$user_info->bio}}
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-8">
                @include('profile.requests')
                @if($team==[])
                    <div class="card d2s-large-post">
                        <div class="card-header" style="border-bottom: 0">
                            Team
                        </div>
                        <div class="card-block">
                            <div class="container">
                                <div class="stat-wrapper">
                                    <div class="row">
                                        <div class="stat-item col-12 col-sm-12 col-md-6" style="padding: 0">
                                            <a href="#" id="create_team" data-toggle="modal"
                                               data-target="#createTeamModal">
                                                <div class="stat-entry">
                                                    <img src="/images/team.png" alt="" class="img-fluid">
                                                    <div class="stat-main-text">Create a Team</div>
                                                    <p class="">For Team Leaders</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="stat-item col-12 col-sm-12 col-md-6" style="padding: 0">
                                            <a href="#" id="join-team" data-toggle="modal" data-target="#joinTeamModal">
                                                <div class="stat-entry">
                                                    <img src="/images/team2.png" alt="" class="img-fluid">
                                                    <div class="stat-main-text">Join a Team</div>
                                                    <p class="">For Team Members</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                @else


                    <div class="card d2s-large-post">
                        <div class="card-header" style="border-bottom: 0">

                            <div class="row">
                                <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                                    <img src="/images/teamlogos/{{ $team->image}}"
                                         class=" img-fluid rounded-circle float-left hidden-sm-down" width="40"
                                         style="margin-right:8px">
                                    <h6 style="padding-top:8px; ">{{$team->name}}</h6>
                                </div>
                                <div class="col-5 col-sm-5 col-md-5 col-lg-5">
                                    <a href="/team/edit/{{$team->slug}}" class="btn btn-primary float-right size-2"
                                       style="margin-top:2px; margin-right: 0 ">Edit
                                        Team Profile</a>
                                </div>
                            </div>


                        </div>
                        <div class="card-block">
                            <h6 class="small">Industry: {{$team->category}}</h6>
                            <h6 class="card-title small">Team Bio:</h6>
                            <p class="card-text small">{{$team->bio}}</p>

                        </div>
                    </div>
                    <div class="card d2s-large-post">
                        <div class="card-header" style="border-bottom: 0">

                            <div class="row">
                                <div class="col-5 col-sm-4 col-md-4 col-lg-5">
                                    <h6 style="padding-top:8px; " class=""
                                        id="mem_num">{{$team->num_members." ".  str_plural('Member', $team->num_members)}}</h6>
                                </div>
                                <div class="col-7 col-sm-8 col-md-8 col-lg-7" id="buttonForInvite">
                                    @if($team->status == "available" && $team->num_members<5)

                                        <a href="#" class="btn btn-primary size-2 float-right"
                                           id="search_team_members" data-toggle="modal"
                                           data-target="#searchTeamModal"
                                           style="margin-top:2px; margin-right: 0 ">Invite Member</a>

                                    @else
                                        <a href="#" class="btn btn-primary size-2 float-right disabled"
                                           style="margin-top:2px; margin-right: 0 ">Team Full</a>

                                    @endif


                                </div>
                            </div>


                        </div>
                        <div class="card-block d2s-team">
                            <div class="row" id="team-members-wrapper">
                                @foreach($team->members as $member)
                                    <div class="col-sm-12 d2s-team-members" id="member{{$member->id}}">
                                        <div class="row">
                                            <div class="col-sm-2 text-center text-sm-left">
                                                <img src="/images/users/{{$member->image}}"
                                                     class="rounded-circle img-fluid " width="100px">
                                            </div>
                                            <div class="col-sm-7 text-center text-sm-left">
                                                <h5>{{$member->user_id==Auth::user()->id ? "You":$member->fullname}}</h5>
                                                <p>
                                                    <small>{{$member->email}}</small>
                                                </p>

                                                @if($member->user_id==Auth::user()->id)

                                                    @if($member->team_role =="")
                                                        <small><a href="/profile/edit#team-role"><i
                                                                        class="fa fa-plus"></i>
                                                                Add
                                                                your role in the team</a></small>
                                                    @else
                                                        <p>{{$member->team_role}} of {{$member->team->name}}</p>
                                                    @endif

                                                @else
                                                    @if($member->team_role =="")
                                                        <small><a href="#" class="request" data-type="team-role"
                                                                  data-request_to="{{$member->id}}"><i
                                                                        class="fa fa-bullhorn"></i>
                                                                Request team role</a></small>
                                                    @else
                                                        <p>{{$member->team_role}}</p>
                                                    @endif
                                                @endif


                                            </div>
                                            <div class="col-sm-3 text-center">

                                                <div class="dropdown">
                                                    <a class="dropdown-toggle"
                                                       href="#" id="dropdownMenuLink"
                                                       data-toggle="dropdown" aria-haspopup="true"
                                                       aria-expanded="false">
                                                        Actions
                                                    </a>

                                                    <div class="dropdown-menu text-center"
                                                         aria-labelledby="dropdownMenuLink">
                                                        @if($team->users_profile_id == Auth::user()->profile->id)

                                                            @if($team->users_profile_id == $member->id)
                                                                <a class="dropdown-item" href="/profile/edit">Edit Your
                                                                    Profile</a>
                                                            @else
                                                                <div class="dropdown-item" id="removeMember"
                                                                     data-member-id="{{$member->id}}"
                                                                     onclick="removeMember(this)">Remove member
                                                                </div>
                                                            @endif


                                                        @elseif($member->user_id==Auth::user()->id)

                                                            <a class="dropdown-item" href="/profile/edit">Edit your
                                                                Profile</a>
                                                            <div class="dropdown-item" id="removeMember"
                                                                 data-team-id="{{$team->id}}"
                                                                 onclick="leaveTeam(this)">Leave this team
                                                            </div>
                                                        @else
                                                            <a class="dropdown-item disabled" href="#">View Profile</a>
                                                        @endif


                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>


@endsection

@section('extraScript')
    <script src="/js/profile.js"></script>
    <script>
        $('#team_tab').on('click', function (e) {
            e.preventDefault();
            $('.tab-info:first').tab('show');
        });
        $('#team_members_tab').on('click', function (e) {
            e.preventDefault();
            $('.tab-info:last').tab('show');
        });
    </script>
@stop