@if(count($results)>0)
    @foreach($results as $result)


        <div class="col-12 d2s-team-members">
            <div class="row">
                <div class="col-sm-2 text-center text-sm-left">
                    <img src="/images/users/{{$result->image}}"
                         class="rounded-circle img-fluid " width="100px">
                </div>
                <div class="col-sm-5 text-center text-sm-left">
                    <h5>{{$result->fullname}}</h5>
                    <small>{{$result->email}}</small>
                </div>

                <div class="col-sm-5" id="searchButtonWrapper{{$result->id}}">
                    @if($result->team_id ==0)
                        <button class="btn btn-success size-2 float-right" id="inviteMember" onclick="invite(this)" data-id="{{$result->id}}">
                            Invite
                        </button>
                    @else
                        <small class=" text-center text-md-right float-md-right">Member of {{$result->team->name}}</small>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@else
    <p><i class="fa fa-frown-o"></i>No results found</p>
@endif
