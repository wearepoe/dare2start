@if(!count($requests) == 0)
    <div class="card d2s-large-post" id="requests-wrapper">
        <div class="card-header" style="border-bottom: 0">

            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                    <h6 style="padding-top:8px; ">Requests</h6>
                </div>
            </div>

        </div>

        <div class="card-block">
            @foreach($requests as $request)
                @php
                    $sendersProfile=App\UsersProfile::find($request->sent_by);
                    $receiversProfile=App\UsersProfile::find($request->received_by);
                @endphp

                @if($request->type == 'join-team')

                    <div class="col-sm-12 d2s-team-members" id="request{{$request->id}}">
                        <div class="row">

                            @if($request->sent_by == $user_info->id)

                                <div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-sm-left">
                                    <img class="rounded-circle img-fluid "
                                         src="/images/users/{{$receiversProfile->image}}"
                                         width="80">
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center text-sm-center text-md-left">


                                    <h5>{{$receiversProfile->fullname}}</h5>
                                    <p>
                                        <small>Is yet to accept your request to join his/her team</small>
                                    </p>
                                </div>

                            @else


                                <div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-sm-left">
                                    <img class="rounded-circle img-fluid "
                                         src="/images/users/{{$sendersProfile->image}}"
                                         width="80">
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center text-sm-center text-md-left">

                                    <h5>{{$sendersProfile->fullname}}</h5>
                                    <p>
                                        <small>Has requested to Join your team</small>
                                    </p>
                                </div>
                            @endif


                            <div class="col-sm-12 col-md-4 col-lg-5">
                                <div class="row">
                                    @if($request->sent_by == $user_info->id)


                                        <div class="col-sm-12 text-center">

                                            <button id="approve-join"
                                                    class="btn btn-danger text-center float-sm-none float-md-right float-lg-right size-2 cancel-join"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-senders-id="{{$sendersProfile->id}}"
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-close"></i>
                                                Cancel
                                            </button>
                                        </div>

                                    @else
                                        <div class="col-sm-12 col-md-12 col-lg-6 text-center">
                                            <button id="approve-join"
                                                    class="btn btn-success text-center float-sm-none float-md-right size-2 approve-join"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-senders-id="{{$sendersProfile->id}}"
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-thumbs-up"></i>
                                                Approve
                                            </button>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-6 text-center">

                                            <button id="approve-join"
                                                    class="btn btn-danger text-center float-sm-none float-md-right size-2 reject-join"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-senders-id="{{$sendersProfile->id}}"
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-thumbs-down"></i>
                                                Reject
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>


                @elseif($request->type == 'invite-to-join')
                    <div class="col-sm-12 d2s-team-members" id="request{{$request->id}}">
                        <div class="row">

                            @if($request->sent_by == $user_info->id)
                                <div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-sm-left">
                                    <img class="rounded-circle img-fluid "
                                         src="/images/users/{{$receiversProfile->image}}"
                                         width="80">
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 text-center text-sm-left text-md-left">
                                    <h5>{{$receiversProfile->fullname}}</h5>

                                    <p>
                                        <small>Is yet to accept your invite</small>
                                    </p>
                                </div>
                            @else

                                <div class="col-12 col-sm-2 col-md-3 col-lg-1 text-center text-sm-left">
                                    <img class="rounded-circle img-fluid "
                                         src="/images/users/{{$sendersProfile->image}}"
                                         width="80">
                                </div>
                                <div class="col-12 col-sm-6 col-md-5 col-lg-6 text-center text-sm-left text-md-left">
                                    <h5>{{$sendersProfile->fullname}}</h5>
                                    <p>
                                        <small>Has sent you an invite to join his/her team</small>
                                    </p>
                                </div>
                            @endif


                            <div class="col-sm-4 col-md-4 col-lg-5 ">
                                <div class="row">
                                    @if($request->sent_by == $user_info->id)


                                        <div class="col-sm-12 text-center">
                                            <button id="approve-join"
                                                    class="btn btn-danger text-center float-sm-none float-md-right float-lg-right size-2 cancel-invite"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-close"></i>
                                                Cancel
                                            </button>
                                        </div>

                                    @else
                                        <div class="col-sm-12 col-md-12 col-lg-6 text-center">
                                            <button id="approve-join"
                                                    class="btn btn-success text-center float-sm-none float-md-right size-2 accept-invite"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-senders-id="{{$sendersProfile->id}}"
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-thumbs-up"></i>
                                                Accept
                                            </button>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-6 text-center">

                                            <button id="approve-join"
                                                    class="btn btn-danger text-center float-sm-none float-md-right size-2 reject-invite"
                                                    style="margin-top:2px; margin-right: 0 "
                                                    data-senders-id="{{$sendersProfile->id}}"
                                                    data-receivers-id="{{$receiversProfile->id}}"
                                                    data-request-id="{{$request->id}}">
                                                <i class="fa fa-thumbs-down"></i>
                                                Reject
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                @endif

            @endforeach
        </div>

    </div>

@endif
