@extends('userlayouts.master')

@section('extraStyles')
    <link rel="stylesheet" href="/css/cropper.css">
@stop


@section('content')


    {{-- image upload --}}
    <div class="modal fade" id="profileImageModal" tabindex="-1" role="dialog" aria-labelledby="joinTeamModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title" id="bioModalLabel">Crop Image</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div id="image-resize" class="image-resize">
                        <img class="img-crop" id="img-crop" src="/images/users/default.jpg">
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" id="save-crop-picture" style="margin-right: 0">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div class="container d2s-detail-container">
        <div class="row">
            <nav class=" hidden-sm-down col-sm-3 col-md-3 col-lg-2 bg-faded sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"> <i class="fa fa-arrow-left"></i> Back to profile</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#basic-information">Basic Information</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#about-me">About Me</a>
                    </li>

                    @if(!$user_info->team == [])
                        <li class="nav-item">
                            <a class="nav-link smoothScroll" href="#team-role">Team Role</a>
                        </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#social-media-handles">Social Media Handles</a>
                    </li>
                </ul>

            </nav>

            <div class="col-12 col-md-9 col-lg-10 _editor-content_">
                <div class="affix-block" id="basic-information">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Basic Information</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="d2s-change-item">
                                <a class="d2s-item-user style-2" href="#" id="selectPictureAlt"
                                   style="position: relative">
                                    <img src="/images/users/{{$user_info->image}}" alt="" id="my-profile-pic">
                                    <div class="uploading-overlay" id="uploading-image">
                                    </div>
                                </a>
                                <a class="btn color-4 size-2 hover-7" id="selectPicture" href="#">Change Picture</a>

                                <form action="#" method="post" style="display: none">
                                    {{csrf_field()}}
                                    <input type="file" id="profile-image-selector" name="profile-image"
                                           style="display: none">
                                    <input type="submit" style="display: none">
                                </form>
                            </div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">
                                <div class="input-col col-sm-9 col-12">
                                    {{csrf_field()}}
                                    <div class="form-group fg_icon focus-2">
                                        <label for="fullname" class="form-label">Full Name</label>
                                        <input class="form-input" type="text" value="{{$user_info->fullname}}"
                                               name="fullname" id="fullname">
                                    </div>
                                </div>

                                <div class="input-col col-sm-3 col-6">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="gender">Gender</label>
                                        <select class="form-input" type="text" name="gender" id="gender">
                                            <option value="male" {{$user_info->gender=="male" ? "selected" : ""}}>Male
                                            </option>
                                            <option value="female" {{$user_info->gender=="female" ? "selected" : ""}}>
                                                Female
                                            </option>
                                        </select>
                                    </div>
                                </div>


                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="dob">Date of Birth</label>
                                        <div class="" id="birthdayForm">
                                        </div>
                                    </div>
                                </div>


                                <div class="input-col col-sm-3 col-6">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="level">Level</label>
                                        <select class="form-input" type="text" name="level" id="level">
                                            <option value="100" {{$user_info->level==100 ? "selected" : ""}}>100
                                            </option>
                                            <option value="200" {{$user_info->level==200 ? "selected" : ""}}>200
                                            </option>
                                            <option value="300" {{$user_info->level==300 ? "selected" : ""}}>300
                                            </option>
                                            <option value="400" {{$user_info->level==400 ? "selected" : ""}}>400
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-col col-sm-9 col-12">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="department">Department</label>
                                        <input class="form-input" type="text" name="department" id="department"
                                               placeholder="Economics Department" value="{{$user_info->department}}">
                                    </div>
                                </div>

                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="residence">Hall of Residence</label>
                                        <select class="form-input" type="text" name="residence" id="residence">
                                            <option value="Non Residence"
                                                    {{$user_info->hall=="Non Residence" ? "selected" : ""}}>
                                                Non Residence
                                            </option>

                                            <option value="Africa Union Hall"
                                                    {{$user_info->hall=="Africa Union Hall" ? "selected" : ""}}>
                                                Africa Union Hall
                                            </option>

                                            <option value="Akuafo Hall"
                                                    {{$user_info->hall=="Akuafo Hall" ? "selected" : ""}}>
                                                Akuafo Hall
                                            </option>

                                            <option value="Alexander Kwapong Hall"
                                                    {{$user_info->hall=="Alexander Kwapong Hall" ? "selected" : ""}}>
                                                Alexander Kwapong Hall
                                            </option>

                                            <option value="Bani Hall"
                                                    {{$user_info->hall=="Bani Hall" ? "selected" : ""}}>
                                                Bani Hall
                                            </option>

                                            <option value="Commonwealth Hall"
                                                    {{$user_info->hall=="Commonwealth Hall" ? "selected" : ""}}>
                                                Commonwealth Hall
                                            </option>

                                            <option value="Elizabeth Sey Hall"
                                                    {{$user_info->hall=="Elizabeth Sey Hall" ? "selected" : ""}}>
                                                Elizabeth Sey Hall
                                            </option>

                                            <option value="Evandy Hall"
                                                    {{$user_info->hall=="Evandy Hall" ? "selected" : ""}}>
                                                Evandy Hall
                                            </option>

                                            <option value="Hilla Limann Hall"
                                                    {{$user_info->hall=="Hilla Limann Hall" ? "selected" : ""}}>
                                                Hilla Limann Hall
                                            </option>

                                            <option value="James Topp Nelson Yankah Hall"
                                                    {{$user_info->hall=="James Topp Nelson Yankah Hall" ? "selected" : ""}}>
                                                James Topp Nelson Yankah Hall
                                            </option>

                                            <option value="Jean Nelson Hall"
                                                    {{$user_info->hall=="Jean Nelson Hall" ? "selected" : ""}}>
                                                Jean Nelson Hall
                                            </option>

                                            <option value="Jubilee Hall"
                                                    {{$user_info->hall=="Jubilee Hall" ? "selected" : ""}}>
                                                Jubilee Hall
                                            </option>

                                            <option value="Legon Hall" {{$user_info->hall=="Legon Hall" ? "selected" : ""}}>
                                                Legon Hall
                                            </option>

                                            <option value="Mensah Sarbah Hall"
                                                    {{$user_info->hall=="Mensah Sarbah Hall" ? "selected" : ""}}>
                                                Mensah Sarbah Hall
                                            </option>

                                            <option value="Volta Hall"
                                                    {{$user_info->hall=="Volta Hall" ? "selected" : ""}}>
                                                Volta Hall
                                            </option>

                                        </select>
                                    </div>
                                </div>


                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">

                                        <small class="float-left text-danger" id="basic-info-errors"
                                               style="font-style: italic"></small>

                                        <button class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"
                                                id="basic-info-save"><i
                                                    class="fa fa-save"></i> Save
                                        </button>
                                        <div class="saving float-right" id="basic-info-save"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="affix-block" id="extra-information">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Extra Information</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">

                                <div class="input-col col-md-6 col-sm-12 col-12">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="phone-number">Phone Number</label>
                                        <input class="form-input" type="text" value="{{$user_info->phone_number}}"
                                               name="phone-number" id="phone-number">
                                    </div>
                                </div>
                                <div class="input-col col-md-6 col-sm-12 col-12">
                                    <div class="form-group focus-2">
                                        <div class="form-group focus-2">
                                            <label class="form-label" for="additional-email">Additional Email</label>
                                            <input class="form-input" type="text"
                                                   value="{{$user_info->additional_email}}"
                                                   name="additional-email" id="additional-email">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    {{csrf_field()}}
                                    <div class="form-group fg_icon focus-2">
                                        <label for="address" class="form-label">Address</label>
                                        <input class="form-input" type="text" value="{{$user_info->address}}"
                                               name="address" id="address">
                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    <div class="form-group fg_icon focus-2">
                                        <label for="favourite-entrepreneur" class="form-label">Favourite
                                            Entrepreneur</label>
                                        <input class="form-input" type="text"
                                               value="{{$user_info->favourite_entrepreneur}}"
                                               name="favourite-entrepreneur" id="favourite-entrepreneur">
                                    </div>
                                </div>


                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <small class="float-left text-danger" id="extra-info-errors"
                                               style="font-style: italic"></small>
                                        <button class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"
                                                id="extra-info-save"><i
                                                    class="fa fa-save"></i> Save
                                        </button>
                                        <div class="saving float-right" id="extra-info-saving"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="affix-block" id="about-me">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">About Me</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">
                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <textarea id="about-me-text" class="form-input"
                                                  placeholder="Something about you">{{$user_info->bio}}</textarea>
                                    </div>
                                </div>

                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <small class="float-left text-danger" id="bio-errors"
                                               style="font-style: italic"></small>

                                        <button id="about-me-save" class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"><i
                                                    class="fa fa-save"></i> Save
                                        </button>
                                        <div class="saving float-right" id="about-me-saving"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if(!$user_info->team ==[])
                    <div class="affix-block" id="team-role">
                        <div class="d2s-large-post">
                            <div class="info-block style-2">
                                <div class="d2s-large-post-align"><h3 class="info-block-label">Team Role</h3></div>
                            </div>
                            <div class="d2s-large-post-align">
                                <div class="row">

                                    <div class="input-col col-sm-12">

                                        <div class="form-group focus-2">
                                            <input id="team-role-text" class="form-input"
                                                   placeholder="Financial Secretary" value="{{$user_info->team_role}}">
                                        </div>
                                    </div>

                                    <div class="input-col col-sm-12">
                                        <div class="form-group focus-2">
                                            <small class="float-left text-danger" id="team-role-errors"
                                                   style="font-style: italic"></small>
                                            <button id="team-role-save"
                                                    class="btn btn-success size-2 float-right disabled"
                                                    style="margin-right: 0"><i
                                                        class="fa fa-save"></i> Save
                                            </button>
                                            <div class="saving float-right" id="team-role-saving"><p>Saving <img
                                                            src="/images/image-loading.gif"></p></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="affix-block" id="social-media-handles">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align">
                                <h3 class="info-block-label">Social Media Handles
                                    <small style="color: red; font-style: italic">(please do not add the '@' symbol)
                                    </small>
                                </h3>
                            </div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #3b5998; color:#fff; width: 50px"><i
                                                        class="fa fa-facebook"></i></div>
                                            <input type="text" class="form-control form-input" id="facebook"
                                                   placeholder="kofi_adams" value="{{$user_info->facebook}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #55acee; color:#fff; width: 50px"><i
                                                        class="fa fa-twitter"></i></div>
                                            <input type="text" class="form-control form-input" id="twitter"
                                                   placeholder="kofi_adams" value="{{$user_info->twitter}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #007bb5; color:#fff; width: 50px"><i
                                                        class="fa fa-linkedin"></i></div>
                                            <input type="text" class="form-control form-input" id="linkedin"
                                                   placeholder="kofi_adams" value="{{$user_info->linkedin}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #fb3958; color:#fff; width: 50px"><i
                                                        class="fa fa-instagram"></i></div>
                                            <input type="text" class="form-control form-input" id="instagram"
                                                   placeholder="kofi_adams" value="{{$user_info->instagram}}">
                                        </div>

                                    </div>
                                </div>


                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <small class="float-left text-danger" id="social-errors"
                                               style="font-style: italic"></small>

                                        <button id="social-save" class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"><i
                                                    class="fa fa-save"></i> Save
                                        </button>

                                        <div class="saving float-right" id="social-saving"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection


@section('extraScript')
    <script src="/js/cropper.min.js"></script>
    <script src="/js/jquery-birthday-picker.js"></script>
    <script src="/js/profile.js"></script>
    <script>

        $("#birthdayForm").birthdayPicker({
            maxAge: 40,
            minAge: 16,
            monthFormat: "long",
            placeholder: true,
            defaultDate: "{{$user_info->date_of_birth}}",
            sizeClass: "span3"
        });

    </script>


@stop