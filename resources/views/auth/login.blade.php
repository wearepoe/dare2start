@php($page_options = App\PageOptions::first())
@extends('layout.master')
@section('content')
    <section id="d2s-hero" style="background-image: url(/images/image3.jpeg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="d2s-overlay-left"></div>
        <div class="container">
            <div class="d2s-intro js-fullheight">
                <div class="d2s-intro-login">
                        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3  col-sm-8 offset-sm-2 animate-box" data-animate-effect="fadeInRight"  style="margin-top: 100px">

                            <div class="card">

                                <div class="card-header bg-primary" style="text-align: center"><h3>Login</h3></div>

                                <div class="card-block">
                                    <form class="form-signin" role="form" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                            <input type="email" name="email" id="inputEmail" class="form-control"
                                                   placeholder="Email address"
                                                   required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif

                                        </div>


                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                            <input type="password" id="inputPassword" class="form-control"
                                                   placeholder="Password" name="password"
                                                   required>


                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div id="remember" class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       value="remember" {{ old('remember') ? 'checked' : '' }}>
                                                Remember me
                                            </label>
                                        </div>

                                        <button class="btn btn-primary float-right" type="submit">Login</button>

                                    </form><!-- /form -->
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-8"><a href="{{ route('password.request') }}"
                                                                 class="forgot-password text-primary">
                                                Forgot the password?
                                            </a></div>
                                        <div class="col-lg-4"><a class="text-success" href="{{ route('register') }}">
                                                Register
                                            </a></div>
                                    </div>

                                </div>
                            </div><!-- /card-container -->
                        </div>
                </div>
            </div>
        </div>
    </section>

@endsection

