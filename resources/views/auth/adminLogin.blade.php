@extends('admin.layout.adminMaster')
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><img src="/images/logo-white.png" class="img-fluid" width="100"></a>
        </div>

        <div class="login-box-body">
            <p class="login-box-msg">Admin Login</p>

            <form class="form-signin" role="form" method="POST"
                  action="{{ route('admin.login.submit') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback ">
                    <input type="email" name="email" id="inputEmail" class="form-control"
                           placeholder="Email address"
                           required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                    @endif

                </div>


                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input type="password" id="inputPassword" class="form-control"
                           placeholder="Password" name="password"
                           required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                    @endif

                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"
                                       value="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <a href="{{ route('password.request') }}">I forgot my password</a><br>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection

