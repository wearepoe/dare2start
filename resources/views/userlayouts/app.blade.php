<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title> Dare2Start </title>
    <link rel="shortcut icon" href="/images/ico.png"/>


    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Lato:300,400,600,700"
          rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="/css/animate.css">
    <!-- Flexslider -->
    <link rel="stylesheet" href="/css/flexslider.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/css/icomoon.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/css/tether.min.css">
    <link rel="stylesheet" href="/css/bootstrap.css">

    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/profile.css">


@yield('extraStyles')


<!-- Modernizr JS -->
    <script src="/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="/js/respond.min.js"></script>
    <![endif]-->


</head>
<body>


<div class="d2s-loader"></div>


<div class="page">

    @include('userlayouts.nav')


    <div id="content-block">
        @yield('content')
    </div>

</div>

<!-- Scripts -->

<script src="/js/jquery.min.js"></script>
<script src="/js/jquery.easing.1.3.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.waypoints.min.js"></script>
<script src="/js/jquery.flexslider-min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/magnific-popup-options.js"></script>
<script src="/js/jquery.smooth-scroll.js"></script>

<script src="/js/main.js"></script>
@yield('extraScript')
<script src="/js/jquery.viewportchecker.min.js"></script>
{{--<script src="/js/global.js"></script>--}}
<script>

    $('.smoothScroll').click(function (event) {
        event.preventDefault();
        var link = this;
        $.smoothScroll({
            scrollTarget: link.hash
        })
    })

</script>

</body>
</html>
