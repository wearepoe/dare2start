<a href="#" class="dropdown-item spaced">
    <div class="row">

        <div class="col-10 col-sm-10" style="padding-right: 0"><p class="text-left text-primary">
                {{$notification ->data['info']['message']}}
            </p>

            <p class="text-left small text-success">
                {{$notification ->data['info']['sender_name']}}
            </p>
        </div>
        <div class="col-2 col-sm-2 text-danger"><i class="fa fa-close"></i></div>

    </div>

</a>
