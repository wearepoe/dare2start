<nav class="navbar navbar-toggleable-md navbar-inverse  bg-primary">


    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
            aria-label="Toggle navigation" style="margin-top: 10px">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="container">
        <a class="navbar-brand" href="#"><img width="90px" height="45px" src="/images/logo-white.png"></a>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item"><a class="nav-link" href="/">Home</a></li>


                <li class="divider"></li>


            </ul>


            <ul class="nav navbar-nav navbar-right">

                @if (Auth::guest())

                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>

                @else

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><i
                                    class="fa fa-user-circle-o"></i> {{ Auth::user()->name }} <span
                                    class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a href="/profile" class="dropdown-item">
                                Your Profile
                            </a>

                            @if($team==[])
                                <a class="dropdown-item" href="#" data-toggle="modal"
                                   data-target="#createTeamModal">Create Team</a>
                                <a class="dropdown-item" href="#" data-toggle="modal"
                                   data-target="#joinTeamModal">Join Team</a>
                            @else
                                <a class="dropdown-item" href="/team/edit/{{$team->slug}}">Edit Your Team</a>
                            @endif


                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="dropdown-item">
                                Logout

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </a>

                        </div>
                    </li>

                    {{--
                        <li class="nav-item dropdown" id="notification-drop-down" data-count="{{count(Auth::user()->profile->unreadNotifications)}}">
                            <a class="nav-link dropdown-toggle " href="#" id="dropdown02"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" ><i
                                        class="fa fa-bell-o"></i> Notifications <span class="badge badge-success" id="notification-count">
                                    {{count(Auth::user()->profile->unreadNotifications)}}
                                </span> </a>

                            @if((count(Auth::user()->profile->unreadNotifications)) >0 )
                                <div class="dropdown-menu scrollable-menu dropdown-menu-right" aria-labelledby="dropdown02" >
                                    @foreach(Auth::user()->profile->unreadNotifications as $notification)
                                        @include('userlayouts.notifications.'.snake_case(class_basename($notification->type)))
                                    @endforeach
                                </div>

                            @else
                                <div class="dropdown-menu scrollable-menu dropdown-menu-right" aria-labelledby="dropdown02">
                                    <a class="dropdown-header text-primary text-center">No Notifications</a>
                                </div>
                            @endif

                        </li>
                        --}}


                @endif

            </ul>
        </div>
    </div>


</nav>
