@if(!is_null($page_options) && $page_options->reg_started == true)

    <section id="d2s-header">
        <div class="container">
            <nav role="navigation">
                <ul class="float-left left-menu">
                    <li><a href="/about">About</a></li>
                    <li><a href="/memories">Memories</a></li>
                    {{--<li><a href="/teams">Teams</a></li>--}}

                    {{-- if there are blog posts--}}
                    @if($page_options->blog_available == true)
                        <li><a href="/blog">Blog</a></li>
                    @endif


                    <li><a href="/contact">Contact</a></li>
                </ul>
                <h1 id="d2s-logo"><a href="/"><img width="120px" height="60px"
                                                   src="/images/logo-white.png"></a></h1>


                <ul class="float-right right-menu">

                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li class="d2s-cta-btn"><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="d2s-cta-btn"><a href="/profile"> <i class="fa fa-user-circle-o"></i> {{ Auth::user()->name }} </a></li>
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout</a>


                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    @endif

                    <li><a href="/faq">FAQ</a></li>
                </ul>

            </nav>
        </div>
    </section>


@else

    <section id="d2s-header">
        <div class="container">
            <nav role="navigation">
                <ul class="float-left left-menu">
                    <li><a href="/about">About</a></li>

                    <li><a href="/memories">Memories</a></li>
                    {{--<li><a href="/teams">Teams</a></li>--}}


                </ul>
                <h1 id="d2s-logo"><a href="/"><img width="120px" height="60px"
                                                   src="/images/logo-white.png"></a></h1>


                <ul class="float-right right-menu">

                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/faq">FAQ</a></li>
                </ul>

            </nav>
        </div>
    </section>



@endif