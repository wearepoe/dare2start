<section id="d2s-subscribe">

	<div class="container">

		@if ($errors->has('sub-email'))
		<h3 style="color: red"><label for="email">{{ $errors->first('sub-email') }}</label></h3>
		@else
		<h3><label for="email">Get more updates on Dare2Start</label></h3>
		@endif

		<form action="/subscribe" method="post">
			{{csrf_field()}}
			<i class="d2s-icon icon-paper-plane"></i>
			<input type="email" class="form-control" placeholder="Enter your email" id="email" name="sub-email" required>
			<input type="submit" value="Subscribe" class="btn btn-primary">
		</form>
	</div>
</section>

