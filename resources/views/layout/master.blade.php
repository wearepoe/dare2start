<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    @include('layout.head')

    @yield('extraStyles')
</head>

<body>


<div class="d2s-loader"></div>


<div id="d2s-page">

    @include('layout.nav')

    @yield('content')

    @include('layout.subscribe')

    @include('layout.footer')

</div>


<script src="/js/jquery.min.js"></script>
<script src="/js/jquery.easing.1.3.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
@yield('extraScripts')
<script src="/js/jquery.waypoints.min.js"></script>
<script src="/js/jquery.flexslider-min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/magnific-popup-options.js"></script>
<script src="/js/main.js"></script>



</body>
</html>

