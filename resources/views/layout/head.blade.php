<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Dare2Start"/>
<meta name="keywords" content="entrepreneurship,challenge,dreams,young,tertiary,competition"/>
<meta name="author" content="poe Studios"/>

{{-- Facebook and Twitter integration --}}
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content=""/>
<meta name="twitter:image" content=""/>
<meta name="twitter:url" content=""/>
<meta name="twitter:card" content=""/>


<meta name="csrf-token" content="{{ csrf_token() }}">


<title>Dare2Start</title>
<link rel="shortcut icon" href="/images/ico.png"/>


<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Lato:300,400,600,700"
      rel="stylesheet">

{{-- Animate.css --}}
<link rel="stylesheet" href="/css/animate.css">
{{-- Flexslider --}}
<link rel="stylesheet" href="/css/flexslider.css">
{{-- Icomoon Icon Fonts--}}
<link rel="stylesheet" href="/css/icomoon.css">
{{-- Magnific Popup --}}
<link rel="stylesheet" href="/css/magnific-popup.css">
{{-- Bootstrap  --}}
<link rel="stylesheet" href="/css/bootstrap.css">

<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/font-awesome.min.css">

{{-- Modernizr JS --}}
<script src="/js/modernizr-2.6.2.min.js"></script>
{{-- FOR IE9 below --}}
<!--[if lt IE 9]>
<script src="/js/respond.min.js"></script>
<![endif]-->