<footer id="d2s-footer">
    <div class="container">
        <div class="row row-bottom-padded-md">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="d2s-footer-widget">
                    <h3>Quick Links</h3>
                    <ul class="d2s-links">
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">About us</a></li>
                        <li><a href="/memories">Memories</a></li>
                        {{--<li><a href="/blog">Blog</a></li>--}}
                        <li><a href="/contact">Contact us</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="d2s-footer-widget">
                    <h3>Partake</h3>
                    <ul class="d2s-links">

                        @if(!is_null($page_options) && $page_options->reg_started == true)

                            @if (Auth::guest())
                                <li><a href="/login">Login</a></li>
                                <li><a href="/register">Register</a></li>
                            @else
                                <li><a href="/profile">{{ Auth::user()->name }} </a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            @endif


                        @endif
                        <li><a href="/volunteer">Volunteer</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="d2s-footer-widget">
                    <h3>Contact Us</h3>
                    <p>
                        <a href="mailto:info@dare2start.com">info@dare2start.com</a> <br>
                        <a href="tel:+233553474720">+233(0)553-474-720</a><br>
                        <a href="tel:+233209136968">+233(0)209-136-968</a>
                    </p>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="d2s-footer-widget">
                    <h3>Follow Us</h3>
                    <ul class="d2s-social">
                        <li><a href="https://twitter.com/dare2startgh"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/Dare2Start-537853503067553/"><i
                                        class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/dare2startgh/"><i class=" fa fa-instagram"></i></a></li>
                        <li><a href="/"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>

    </div>
    <div class="d2s-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="d2s-left">
                        <small>&copy; 2017 <a href="index.php">Dare2Start</a> All Rights Reserved.</small>
                    </p>
                    <p class="d2s-right">
                        <small class="d2s-right">Built with <i class="fa fa-heart"></i> by <a href="https://www.wearepoe.com"
                                                                         target="_blank">Poe
                                Studios</a></small>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>