<div class="row">

    <div class="col-lg-12">
        <h3 class="d2s-label">Search</h3>
    </div>


    <div class="col-lg-12">
        <form action="/search" method="get">
            {{csrf_field()}}

            <div class="input-group">
                <input type="text" class="form-control search_input" placeholder="Search">
                <span class="input-group-btn">
                    <input type="submit" class="btn btn-primary search_submit" value="Go!">
                </span>
            </div>
        </form>
    </div>
</div>