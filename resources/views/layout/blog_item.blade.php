<div class="col-lg-6 col-md-6 col-sm-12">
    <a href="/blog/{{$blog_post->id}}">
        <div class="d2s-blog ">
            <figure>

                <img src="/images/person7.jpg"
                     alt="Free HTML5 Bootstrap Template by FREEHTML5.co"
                     class="img-fluid"/>
            </figure>
            <h3 class="d2s-name text-justify">{{$blog_post->title}}</h3>
            <p class="d2s-name text-justify"><i>Posted on: {{$blog_post->created_at->toFormattedDateString()}}</i></p>
            <p class="d2s-bio text-justify">
                {{str_limit($blog_post->body, $limit = 350, $end = '...')}}</p>

        </div>
    </a>
</div>