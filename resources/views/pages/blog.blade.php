@extends('layout.master')

@section('content')
    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/full_image_1.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Our Blog</h2>
                        <h3>News, Updates, Events</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <div id="d2s-features-2">
        <div class="container">

            <div class="row">
                <div class="col-lg-8">
                    <div class="row">

                        @foreach($blog_posts as $blog_post)

                            @include('layout.blog_item')
                        @endforeach

                    </div>

                </div>

                <div class="col-lg-4">
                    @include('layout.sidebar')
                </div>
            </div>

        </div>
    </div>

@endsection