@extends('layout.master')

@section('content')



    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/img5.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Contact Us</h2>
                        <h3>Communicate with us directly</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="d2s-features">

        <div class="container">

            <div class="row row-bottom-padded-sm">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h2 class="d2s-lead ">Send us a message</h2>
                </div>
            </div>

            <form action="/contact" method="post">

                {{csrf_field()}}

                <div class="col-lg-8 offset-lg-2">

                    <div class="row ">

                        <div class="col-lg-12">
                            <div class="form-group" style="padding-left: 10px">
                                @if(count($errors))
                                    <p>There were some errors in your submission</p>
                                    @foreach($errors->all() as $error)
                                        <li style="color: red">{{$error}}</li>

                                    @endforeach

                                @endif
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="full_name">Full Name <span class="req">*</span></label>
                                <input type="text" class="form-control" name="full_name" value="{{ old('full_name') }}"
                                       id="full_name" required>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email <span class="req">*</span></label>
                                <input type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" id="email" required>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="phone_number">Phone Number</label>
                                <input type="tel" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}" id="phone_number">
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="message">Message <span class="req">*</span></label>

                                <textarea name="message" class="form-control" id="" rows="7"
                                          value="{{ old('message') }}"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group float-right">
                                <input type="submit" value="Send Message" class="btn btn-primary">
                            </div>
                        </div>


                    </div>
                </div>
            </form>


            <div class="col-lg-8 offset-lg-2 text-left">
                <h3>Other Contacts</h3>
                <p><i class="icon-envelope"></i> <b>Email:</b><a href="mailto:info@dare2start.com">info@dare
                        2start.com</a><br>
                    <i class="icon-phone"></i> <b>Hotline:</b> <a href="tel:+233553474720">+233(0)553-474-720</a><br>
                    <i class="icon-phone"></i> <b>Samuel Kofi Duah:</b> <a
                            href="tel:+233209136968">+233(0)209-136-968</a>
                </p>

            </div>
        </div>
    </section>

@endsection


