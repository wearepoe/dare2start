@extends('layout.master')

@section('content')
    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/collage2.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Our Memories</h2>
                        <h3>All that made our first edition a success</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="d2s-features"></section>

@endsection