@extends('layout.master')

@section('content')



    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/img5.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Volunteer</h2>
                        <h3>Make Dare2Start a success</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="d2s-features">

        <div class="container">

            <div class="row row-bottom-padded-sm">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h2 class="d2s-lead ">We would love to have your support</h2>
                </div>
            </div>

            <form action="/volunteer" method="post">

                {{csrf_field()}}

                <div class="col-lg-8 offset-lg-2">

                    <div class="row ">

                        <div class="col-lg-12">
                            <div class="form-group" style="padding-left: 10px">
                                @if(count($errors))
                                    <p>There were some errors in your submission</p>
                                    @foreach($errors->all() as $error)
                                        <li style="color: red">{{$error}}</li>

                                    @endforeach

                                @endif
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="fullname">Full Name <span class="req">*</span></label>
                                <input type="text" class="form-control" name="fullname"
                                       value="{{ old('fullname') }}" id="fullname" required>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email <span class="req">*</span></label>
                                <input type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" id="email" required>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="phone_number">Phone Number <span class="req">*</span></label>
                                <input type="tel" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}" id="phone_number" required>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="section">Which section do you want to volunteer for <span
                                            class="req">*</span></label>

                                <select id="section" class="form-control" name="section">
                                    <option value="Planning/Organising">Planning/Organising</option>
                                    <option value="Ushering">Ushering</option>
                                    <option value="Publicity">Publicity</option>
                                    <option value="Media and photography">Media and photography (must have some level of
                                        expertise)
                                    </option>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="other_info"> Other information</label>
                                <textarea name="other_info" class="form-control" id="other_info" rows="7" value="{{ old('message') }}"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group float-right">
                                <input type="submit" value="Submit" class="btn btn-primary">
                            </div>
                        </div>


                    </div>
                </div>
            </form>

        </div>
    </section>

@endsection


