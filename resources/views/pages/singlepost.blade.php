@extends('layout.master')

@section('content')
    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/full_image_1.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">
                    <div class="col-lg-12">

                        <div class="d2s-center-position text-center">
                            <h2>{{$blog_post->title}}</h2>

                            <h3>{{$blog_post->created_at->toFormattedDateString()}}</h3>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <div id="d2s-features-2">
        <div class="container">

            <div class="row">
                <div class="col-lg-8">

                    <div class="d2s-blog ">
                        <img src="/images/person7.jpg"
                             alt="Free HTML5 Bootstrap Template by FREEHTML5.co"
                             class="img-fluid"/>

                        <div class="container">
                            <h3 class="d2s-name text-left">{{$blog_post->title}}</h3>
                            <p class="d2s-bio text-left">
                                {{$blog_post->body}}</p>
                        </div>

                    </div>


                </div>

                <div class="col-lg-4">
                    @include('layout.sidebar')
                </div>
            </div>

        </div>
    </div>

@endsection