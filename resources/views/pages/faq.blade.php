@extends('layout.master')

@section('content')
    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/full_image_1.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Frequently Asked Questions</h2>
                        <h3>All frequently asked question being answered</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>



    <div id="d2s-faqs">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <ul class="fh5co-faq-list">
                        <li class="animate-box">
                            <h2>What is Dare2Start?</h2>
                            <p>Dare2Start is an exciting, fun-filled, educative and mind-blowing
                                entrepreneurship competition designed to help solve the graduate unemployment problem in
                                the country by empowering and encouraging the entrepreneurial mindset. The objective is
                                to ignite the entrepreneurial spirit and mind-set in about 25,000 students on the
                                University of Ghana campus and provide training and support to about 400 students on how
                                to run their businesses. <a href="/about">Click here to learn more</a></p>
                        </li>
                        <li class="animate-box">
                            <h2>How do i register?</h2>
                            <p>When registration begins links will be provided where participants can register and
                                partake in the competition. You can subscribe to our news letter to get updates on the
                                competition</p>
                        </li>
                        <li class="animate-box">
                            <h2>Is registration free?</h2>
                            <p>Yes registration is free without any charges</p>
                        </li>
                        <li class="animate-box">
                            <h2>I have technical problem, who do I email?</h2>
                            <p>Send an email to development@dare2start.com or fill the send a message form on the
                                <a href="/contact">Contact us</a> page and it will be directed to the development
                                team</p>
                        </li>
                        <li class="animate-box">
                            <h2>Can I have an email that is already been used?</h2>
                            <p>You cannot have an email that is already used</p>
                        </li>
                        <li class="animate-box">
                            <h2>How do i create a team</h2>
                            <p>After logging in for the first time you will see a panel with the options to create a
                                team or join a team. To create a team click on create a team and fill in the required
                                details.</p>
                        </li>
                        <li class="animate-box">
                            <h2>How do i join a team</h2>
                            <p>After logging in for the first time you will see a panel with the options to create a
                                team or join a team. To join a team you will have to search for a team in the search box
                                that pops
                                up. A request will be sent to the team leader. The leader will then accept or reject
                                your request</p>
                        </li>
                        <li class="animate-box">
                            <h2>How do i invite a team member</h2>
                            <p>After logging in for the first time you will see a panel with the options to create a
                                team or join a team. To join a team you will have to search for a team in the search box
                                that pops
                                up. A request will be sent to the team leader. The leader will then accept or reject
                                your request</p>
                        </li>

                        <li class="animate-box">
                            <h2>I have forgotten my password</h2>
                            <p>You can recover your password via the email you used in the registration process <a
                                        href="/password/reset">Click here to reset your password</a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection