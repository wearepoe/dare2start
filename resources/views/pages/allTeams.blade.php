@extends('layout.master')

@section('content')


    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/logo-wide.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Teams</h2>
                        <h3>{{count($teams)}} registered {{str_plural('team', count($teams))}}</h3>

                    </div>
                </div>
            </div>
        </div>
        <div class="d2s-learn-more ">
            <a href="#" class="scroll-btn">
                <span class="arrow"><i class="icon-chevron-down"></i></span>
            </a>
        </div>
    </section>


    <div id="d2s-features">
        <div class="col-sm-8 offset-sm-2">
            <div class="teams-wrapper">
                @foreach($teams as $team)
                    <div class="row teams animate-box fadeInUp animated">
                        <div class="col-md-12 col-lg-2 text-center">
                            <img src="images/teamlogos/{{$team->image}}" alt="{{snake_case($team->name)}}_logo"
                                 class="img-fluid rounded-circle" width="100px">
                        </div>
                        <div class="col-md-12 col-lg-5 text-center text-lg-left">
                            <h4 class="">{{$team->name}}</h4>
                            <p>Industry: {{$team->category}}</p>
                            <p> {{$team->num_members." ".  str_plural('Member', $team->num_members)}}</p>
                        </div>
                        <div class="col-sm-12 col-lg-5 container" style="padding: 0">
                            <div class="img-wrapper text-center">
                                @foreach($team->members as $member)
                                    <img src="images/users/{{$member->image}}"
                                         class="img-fluid rounded-circle mem-image">
                                @endforeach

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="pagination-holder">{{$teams->links()}}</div>


        </div>

    </div>

@endsection

@section('extraScripts')

@stop