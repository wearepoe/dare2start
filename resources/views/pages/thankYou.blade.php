@extends('layout.master')

@section('extraStyles')
    <link type="text/css" href="/css/chocolat.css">
@stop

@section('content')

    <section id="d2s-hero" style="background-image: url('/images/image4.jpeg');"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro js-fullheight">
                <div class="d2s-intro-text">
                    <div class="d2s-center-position animate-box">
                        <h2 class="">Thank you for sending your request to volunteer</h2>
                        <p>we will respond to it soon.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="d2s-learn-more ">
            <a href="#" class="scroll-btn">
                <span class="arrow"><i class="icon-chevron-down"></i></span>
            </a>
        </div>
    </section>


@section('extraScripts')

@stop

@endsection




