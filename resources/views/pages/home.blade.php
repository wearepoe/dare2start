@extends('layout.master')

@section('extraStyles')
    <link type="text/css" href="/css/chocolat.css">
@stop

@section('content')

    <section id="d2s-hero" style="background-image: url('/images/image4.jpeg');"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro js-fullheight">
                <div class="d2s-intro-text">
                    <div class="d2s-center-position animate-box">
                        <h2 class="">Empowering entrepreneurs with creative minds </h2>
                        <p class="">

                            {{--
                            <a href="#playvideo"
                               class="btn btn-outline btn-video scroll-btn">
                                <i class="icon-play2"></i> Watch video</a>--}}

                            @if(!is_null($page_options) && $page_options->reg_started== true)
                                <a href="/register" class="btn btn-primary">Register</a>
                            @else
                                <a href="/about" class="btn btn-primary">Learn more</a>
                            @endif


                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="d2s-learn-more ">
            <a href="#" class="scroll-btn">
                <span class="arrow"><i class="icon-chevron-down"></i></span>
            </a>
        </div>
    </section>

    {{-- END #d2s-hero --}}

    <div id="d2s-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 text-center">
                    <h2 class="d2s-lead ">What is Dare2Start?</h2>
                    <p class="d2s-sub-lead ">Dare2start is an exciting, fun-filled, educative and mind-blowing
                        entrepreneurship competition designed to help solve the issue of graduate unemployment of the
                        youthful population all over Africa by empowering and encouraging an entrepreneurial
                        mind-set.</p>
                </div>


                @if(!is_null($sponsors))

                    <div class="sponsors row">
                        @foreach($sponsors as $sponsor)
                            <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                                <div class="d2s-feature">
                                    <div class="d2s-icon">
                                        <img class="img-fluid" src="/images/sponsors/{{$sponsor->image_name}}"
                                             alt="{{$sponsor->name}}">
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                @endif

            </div>


        </div>
    </div>

    {{-- END #d2s-features -- }}

    {{--
        <div id="playvideo">
            <div id="d2s-info" style="background: url('/images/image1.jpg') no-repeat center; background-size: cover">

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="d2s-watch-now ">
                                <a href="https://www.youtube.com/watch?v=JQbt0TQWyFo"
                                   class="popup-vimeo btn-video play-btn">
                                    <span class="play"><i class="icon-play2"></i></span>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



    <div id="{{count($blog_posts)?"d2s-features-3": "d2s-info"}}">
        <div class="container">
            <div class="row row-bottom-padded-lg">
                <div class="col-lg-5">
                    <figure>
                        <img src="/images/img_3.jpg" alt="Free HTML5 Bootstrap Template by FREEHTML5.co"
                             class="img-fluid">
                    </figure>
                </div>
                <div class="col-lg-6">
                    <h2 class="d2s-lead ">Congratulations</h2>
                    <p class="d2s-sub-lead ">To team Aceup for wining the 2016 edition of Dare2Start.
                        with their business idea on helping SME’s with accounting services and business solutions at low
                        cost with a two-month free trial period.</p>
                </div>
            </div>
        </div>
    </div>
    --}}

    @if(count($blog_posts))
        <section id="d2s-info">
            <div class="container">
                <h2 class="d2s-lead text-center">Our blog</h2>

                <div class="row">

                    @foreach($blog_posts as $blog_post)
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <a href="#">
                                <div class="d2s-blog">
                                    <figure>
                                        <img src="/images/person7.jpg"
                                             alt="Free HTML5 Bootstrap Template by FREEHTML5.co"
                                             class="img-fluid"/>
                                    </figure>
                                    <h3 class="d2s-name  text-justify">{{$blog_post->title}}</h3>
                                    <p class="d2s-bio  text-justify">
                                        {{str_limit($blog_post->body, $limit = 150, $end = '...')}}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>
            </div>
        </section>
        {{-- END #d2s-features-2 --}}

    @endif

    <section id="d2s-testimonials">
        <div class="container">
            <div class="row row-bottom-padded-sm">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h2 class="d2s-lead ">Responses from some participants</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center ">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <blockquote>
                                    <p>&ldquo;The training pushed us to think harder and organise thought we hadn't
                                        already written down&rdquo;</p>
                                    <p><cite>&mdash; Farm Leaf</cite></p>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>
                                    <p>&ldquo;The training introduced us to the practical realities, was interractive
                                        and encouraging, and dared us to start.&rdquo;</p>
                                    <p><cite>&mdash; Team Steamo</cite></p>
                                </blockquote>
                            </li>
                            <li>
                                <blockquote>
                                    <p>&ldquo;It was an eye opening and fun meeting. We were given more information on
                                        how to go about a business.&rdquo;</p>
                                    <p><cite>&mdash; The Movers</cite></p>
                                </blockquote>
                            </li>
                        </ul>
                    </div>
                    <div class="flexslider-controls">
                        <ol class="flex-control-nav">
                            <li class=""><img src="/images/farm-leaf.jpg"
                                              alt="https://twitter.com/farmleaf"></li>
                            <li class=""><img src="/images/steamo.jpg"
                                              alt="https://www.facebook.com/STEAMO4ALL/"></li>
                            <li class=""><img src="/images/default.jpg"
                                              alt="#"></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

@section('extraScripts')
<script>
    $(document).ready(function(){

    })
</script>
@stop

@endsection




