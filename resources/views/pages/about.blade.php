@extends('layout.master')

@section('content')


    <section id="d2s-hero" class="no-js-half-height" style="background-image: url(/images/logo-wide.jpg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="container">
            <div class="d2s-intro no-js-fullheight">
                <div class="d2s-intro-text">

                    <div class="d2s-center-position">
                        <h2>Dare2Start</h2>
                        <h3>Where the whole idea came from</h3>

                    </div>
                </div>
            </div>
        </div>
        <div class="d2s-learn-more ">
            <a href="#" class="scroll-btn">
                <span class="arrow"><i class="icon-chevron-down"></i></span>
            </a>
        </div>
    </section>


    <div id="d2s-features">
        <div class="container">

            <h2 class="d2s-lead">The Reason</h2>
            <p class="d2s-sub-lead">Although Ghana is politically stable, given the high levels of unemployment
                statistics within the country, this stability is not enough to guarantee that such a peaceful
                environment will forever continue. The Arab Spring which started in North Africa and the Middle
                East was as a result of unemployment among the youth. Likewise, in some parts of Africa,
                specifically Ghana, some young people mostly unemployed have resorted to using guns, knives and
                other weapons to demand for what they think life owes them; by attacking unsuspecting
                individuals and organizations. Apparently, this clearly points to the fact that, we are greatly
                at risk as a Nation and Continent if measures are not put in place to address this menace.
                For example, out of the 289,539 graduates that were deployed by the National Service Secretariat
                (NSS), from 2011 to 2015, only 5,000 were absorbed into the formal sector. This figure
                represents only 1.73% of graduates.</p>

            <div class="row row-top-padded-md">
                <div class="col-md-4 col-sm-6 col-xs-12 animate-box fadeInUp animated">
                    <div class="d2s-feature">
                        <div class="d2s-icon">
                            <i class="icon-graduation-cap"></i>
                        </div>
                        <h3>The increasing rate of graduate unemployment</h3>
                        <p>Although Ghana is politically stable, it is not enough to guarantee that such a peaceful
                            environment will forever continue, given the high levels of unemployment statistics within
                            the country.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 animate-box fadeInUp animated">
                    <div class="d2s-feature">
                        <div class="d2s-icon">
                            <i class="icon-anchor"></i>
                        </div>
                        <h3>Building the entrepreneurship spirit</h3>
                        <p>The competition seeks to stir an atmosphere for entrepreneurship. The purpose of
                            this ‘agenda’ is to make entrepreneurship attractive to the African youth. In as much
                            as parents, pastors, opinion leaders alike will say it is Ok to be a doctor, or a lawyer,
                            or an architect so should it be said of anyone who wants to become an entrepreneur.</p>
                    </div>
                </div>
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-4 col-sm-6 col-xs-12 animate-box fadeInUp animated">
                    <div class="d2s-feature">
                        <div class="d2s-icon">
                            <i class="icon-flag"></i>
                        </div>
                        <h3>The need for Training</h3>
                        <p>The road to becoming a world-class entrepreneur is long, winding and undulating path that
                            requires mentorship, coaching and training.We seek to develop students’ creativity and equip
                            them with skills to enable them build profitable businesses in the near future.</p>
                    </div>
                </div>
                <div class="clearfix visible-sm-block"></div>
            </div>

            <h2 class="d2s-lead ">The Objective</h2>

            <p class="d2s-sub-lead">The main objective of the project is to ignite the entrepreneurial spirit and
                mind-set of the youthful population in Africa, mainly between the ages of 18-35.
                <br>
                The objective of the first edition was to ignite the entrepreneurial spirit and
                mind-set in about 25,000 students on the University of Ghana campus.
                In addition, the project engaged and trained 270 students on how to run their businesses.
                Concurrently, the project helped at least 20 students to launch their businesses.
            </p>

        </div>

    </div>

    {{--
        <div id="d2s-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h2 class="d2s-lead ">Watch our Video to get to know more about us</h2>
                    </div>
                    <div class="col-md-7">
                        <figure>
                            <img src="/images/img_4.jpg" alt="Free HTML5 Bootstrap Template by FREEHTML5.co"
                                 class="img-fluid">
                        </figure>
                    </div>
                </div>
            </div>
        </div>

    --}}
@endsection