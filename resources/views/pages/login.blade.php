@php($page_options = App\PageOptions::first())
@extends('layout.master')
@section('content')
    <section id="d2s-hero" style="background-image: url(/images/image3.jpeg);"
             data-next="yes">
        <div class="d2s-overlay"></div>
        <div class="d2s-overlay-left"></div>
        <div class="container">
            <div class="d2s-intro js-fullheight">
                <div class="d2s-intro-login ">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="container">
                                <h2>Join the Fun</h2>
                            </div>
                        </div>
                        <div class="col-lg-4 animate-box" data-animate-effect="fadeInRight">

                            <div class="card">

                                <div class="card-header bg-primary" style="text-align: center"><h3>Register</h3></div>

                                <div class="card-block">
                                    <form class="form-signup" role="form" method="POST" action="{{ route('register') }}">
                                        {{ csrf_field() }}


                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">

                                            <input type="text" name="name" id="inputEmail" class="form-control"
                                                   placeholder="Name" value="{{ old('name') }}"
                                                   required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif

                                        </div>

                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

                                            <input type="email" name="email" id="inputEmail" class="form-control"
                                                   placeholder="Email address"  value="{{ old('email') }}"
                                                   required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif

                                        </div>


                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">

                                            <input type="password" id="inputPassword" class="form-control"
                                                   placeholder="Password" name="password"
                                                   required>


                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="form-group">

                                            <input type="password" id="inputPassword" class="form-control"
                                                   placeholder="Confirm password" name="password_confirmation"
                                                   required>

                                        </div>


                                        <button class="btn btn-primary float-right" type="submit">Register</button>

                                    </form><!-- /form -->
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-8"><a class="text-success" href="{{ route('login') }}">
                                                Already having an account?
                                            </a></div>
                                    </div>

                                </div>
                            </div><!-- /card-container -->
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

