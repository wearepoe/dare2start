@extends('userlayouts.master')

@section('extraStyles')
    <link rel="stylesheet" href="/css/cropper.css">
@stop


@section('content')


    {{-- image upload --}}
    <div class="modal fade" id="teamLogoModal" tabindex="-1" role="dialog" aria-labelledby="uploadTeamLogoModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title" id="bioModalLabel">Crop Image</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div id="image-resize" class="image-resize">
                        <img class="img-crop" id="img-crop" src="/images/users/default.jpg">
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" id="save-crop-picture" style="margin-right: 0">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div class="container d2s-detail-container">
        <div class="row">
            <nav class=" hidden-sm-down col-sm-3 col-md-3 col-lg-2 bg-faded sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"> <i class="fa fa-arrow-left"></i> Back to profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#basic-information">Team Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#extra-information">Extra Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoothScroll" href="#social-media-handles">Team Links</a>
                    </li>
                </ul>

            </nav>

            <div class="col-12 col-md-9 col-lg-10 _editor-content_">
                <div class="affix-block" id="basic-information">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Basic Information</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">
                                <div class="input-col col-md-12 col-lg-4">
                                    <div class="d2s-change-logo">
                                        <div class="d2s-team-logo">
                                            <a class=" selectPicture style-2" href="#" id="selectPictureAlt"
                                               style="position: relative">
                                                <img src="/images/teamlogos/{{$team->image}}" alt="" id="my-team-logo"
                                                     class="img-fluid">
                                                <div class="uploading-overlay" id="uploading-image"></div>
                                            </a>
                                        </div>

                                        <div>
                                            <a class="btn color-4 size-2 hover-7 selectPicture" id="selectPicture"
                                               href="#">Change Logo</a>
                                        </div>

                                        <form action="#" method="post" style="display: none">
                                            {{csrf_field()}}
                                            <input type="file" id="team-logo-selector" name="profile-logo"
                                                   style="display: none">
                                            <input type="submit" style="display: none">
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-8">
                                    <div class="row">
                                        <div class="input-col col-sm-12 col-12">
                                            {{csrf_field()}}
                                            <div class="form-group fg_icon focus-2">
                                                <label for="teamName" class="form-label">Team Name</label>
                                                <input class="form-input" type="text" value="{{$team->name}}"
                                                       name="teamName" id="teamName">
                                            </div>
                                        </div>
                                        <div class="input-col col-sm-12 col-12">
                                            <div class="form-group focus-2">
                                                <label class="form-label" for="teamIndustry">Industry</label>
                                                <select class="form-input" type="text" name="teamIndustry"
                                                        id="teamIndustry">
                                                    <option {{$team->category=="Advertising / Public Relations" ? "selected" : ""}} value="Advertising / Public Relations">
                                                        Advertising / Public
                                                        Relations
                                                    </option>
                                                    <option {{$team->category=="Agriculture" ? "selected" : ""}}  value="Agriculture">
                                                        Agriculture
                                                    </option>
                                                    <option {{$team->category=="Architecture Services" ? "selected" : ""}}  value="Architecture Services">
                                                        Architecture Services
                                                    </option>
                                                    <option {{$team->category=="Banking and Finance" ? "selected" : ""}}  value="Banking and Finance">
                                                        Banking and Finance
                                                    </option>
                                                    <option {{$team->category=="Construction" ? "selected" : ""}}  value="Construction">
                                                        Construction
                                                    </option>
                                                    <option {{$team->category=="Education" ? "selected" : ""}}  value="Education">
                                                        Education
                                                    </option>
                                                    <option {{$team->category=="Fashion Design" ? "selected" : ""}}  value="Fashion Design">
                                                        Fashion Design
                                                    </option>
                                                    <option {{$team->category=="Food and Beverage" ? "selected" : ""}}  value="Food and Beverage">
                                                        Food and Beverage
                                                    </option>
                                                    <option {{$team->category=="Health and Hospitality" ? "selected" : ""}}  value="Health and Hospitality">
                                                        Health and Hospitality
                                                    </option>
                                                    <option {{$team->category=="IT" ? "selected" : ""}}  value="IT">IT
                                                    </option>
                                                    <option {{$team->category=="Manufacturing" ? "selected" : ""}}  value="Manufacturing">
                                                        Manufacturing
                                                    </option>
                                                    <option {{$team->category=="Marketing and Sales" ? "selected" : ""}}  value="Marketing and Sales">
                                                        Marketing and Sales
                                                    </option>
                                                    <option {{$team->category=="Tourism" ? "selected" : ""}}  value="Tourism">
                                                        Tourism
                                                    </option>
                                                    <option {{$team->category=="Transport" ? "selected" : ""}}  value="Transport">
                                                        Transport
                                                    </option>
                                                    <option {{$team->category=="Waste Management" ? "selected" : ""}}  value="Waste Management">
                                                        Waste Management
                                                    </option>
                                                    <option {{$team->category=="Other" ? "selected" : ""}}  value="Other">
                                                        Other
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="input-col col-sm-12 col-12">
                                            <div class="form-group focus-2">
                                                <label class="form-label" for="teamBio">Team Bio</label>
                                                <div class="form-group focus-2">
                                        <textarea id="teamBio" class="form-input"
                                                  placeholder="Something about the team"
                                                  name="teamBio">{{$team->bio}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-col col-sm-12">
                                            <div class="form-group focus-2">
                                                <small class="float-left text-danger" id="basic-details-errors" style="font-style: italic"></small>

                                                <button class="btn btn-success size-2 float-right disabled"
                                                        style="margin-right: 0" id="basic-details-save">
                                                    <i class="fa fa-save"></i> Save
                                                </button>

                                                <div class="saving float-right" id="team-basic-info-saving"><p>Saving
                                                        <img
                                                                src="/images/image-loading.gif"> </p></div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="affix-block" id="extra-information">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Extra Information</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">

                                <div class="input-col col-sm-12 col-12">
                                    <div class="form-group focus-2">
                                        <label class="form-label" for="teamNumber">Phone Number</label>
                                        <input class="form-input" type="text" value="{{$team->phone_number}}"
                                               name="teamNumber" id="teamNumber">
                                    </div>
                                </div>
                                <div class="input-col col-sm-12 col-12">
                                    <div class="form-group focus-2">
                                        <div class="form-group focus-2">
                                            <label class="form-label" for="teamEmail">Team Email</label>
                                            <input class="form-input" type="text" value="{{$team->email}}"
                                                   name="teamEmail" id="teamEmail">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    {{csrf_field()}}
                                    <div class="form-group fg_icon focus-2">
                                        <label for="teamMission" class="form-label">Mission</label>
                                        <textarea class="form-input" type="text"
                                                  name="teamMission" id="teamMission">{{$team->mission}}</textarea>
                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    {{csrf_field()}}
                                    <div class="form-group fg_icon focus-2">
                                        <label for="teamVision" class="form-label">Vision</label>
                                        <textarea class="form-input" type="text"
                                                  name="teamVision" id="teamVision">{{$team->vision}}</textarea>
                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    {{csrf_field()}}
                                    <div class="form-group fg_icon focus-2">
                                        <label for="teamObjectives" class="form-label">Objectives</label>
                                        <textarea class="form-input" type="text"
                                                  name="teamObjectives"
                                                  id="teamObjectives">{{$team->objectives}}</textarea>
                                    </div>
                                </div>


                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <small class="float-left text-danger" id="extra-info-errors" style="font-style: italic"></small>

                                        <button class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"
                                                id="teamExtraInfoSave"><i
                                                    class="fa fa-save"></i> Save
                                        </button>

                                        <div class="saving float-right" id="team-extra-info-saving"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="affix-block" id="social-media-handles">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Team Links</h3>
                            </div>
                        </div>
                        <div class="d2s-large-post-align">
                            <div class="row">

                                <div class="input-col col-sm-12 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #3b5998; color:#fff; width: 50px"><i
                                                        class="fa fa-globe"></i></div>
                                            <input type="text" class="form-control form-input" id="teamWebsite"
                                                   placeholder="www.example.com" value="{{$team->website}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #3b5998; color:#fff; width: 50px"><i
                                                        class="fa fa-facebook"></i></div>
                                            <input type="text" class="form-control form-input" id="teamFacebook"
                                                   placeholder="facebook page name" value="{{$team->facebook}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #55acee; color:#fff; width: 50px"><i
                                                        class="fa fa-twitter"></i></div>
                                            <input type="text" class="form-control form-input" id="teamTwitter"
                                                   placeholder="twitter page name" value="{{$team->twitter}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #007bb5; color:#fff; width: 50px"><i
                                                        class="fa fa-linkedin"></i></div>
                                            <input type="text" class="form-control form-input" id="teamLinkedin"
                                                   placeholder="linkedin page name" value="{{$team->linkedin}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="input-col col-sm-6 col-12">
                                    <div class="form-group focus-2">
                                        <div class="input-group">
                                            <div class="input-group-addon"
                                                 style=" background-color: #fb3958; color:#fff; width: 50px"><i
                                                        class="fa fa-instagram"></i></div>
                                            <input type="text" class="form-control form-input" id="teamInstagram"
                                                   placeholder="instagram page name" value="{{$team->instagram}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="input-col col-sm-12">
                                    <div class="form-group focus-2">
                                        <small class="float-left text-danger" id="social-errors" style="font-style: italic"></small>

                                        <button id="teamLinkSaveBtn" class="btn btn-success size-2 float-right disabled"
                                                style="margin-right: 0"><i
                                                    class="fa fa-save"></i> Save
                                        </button>

                                        <div class="saving float-right" id="social-saving"><p>Saving <img
                                                        src="/images/image-loading.gif"></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                {{--
                <div class="affix-block text-right" >

                    <a href="/delete/team" class="text-danger"><i class="fa fa-trash"></i> Delete Team</a>
                </div>
                --}}
            </div>
        </div>
    </div>
@endsection


@section('extraScript')
    <script src="/js/cropper.min.js"></script>
    <script src="/js/team.js"></script>


@stop