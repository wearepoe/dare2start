@extends('userlayouts.master')


@section('extraStyles')
    <link rel="stylesheet" href="/css/teamprofile.css">
    <link rel="stylesheet" href="/css/cropper.css">
@stop

@section('content')


    <div class="container">

        <div class="top-info animate-box">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-8 offset-xs-2">

                    <div class="card-image-holder">
                        <img src="/images/teamlogos/{{$team->image}}" class=" img-fluid  card-img-top rounded"
                             id="profile-image">
                    </div>

                    <div>
                        <div class="bio">
                            <h2>{{$team->name}}</h2>
                            <h4>Edit</h4>
                        </div>
                    </div>

                </div>


                <div class="col-lg-8 col-md-6 col-sm-6">

                </div>
            </div>
        </div>

        <div class="other-info animate-box">
            <div class="row">

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-8 offset-xs-2">

                </div>


                <div class="col-lg-8 col-md-6 col-sm-6">
                    {{--
                                     @if($team == [])
                                         <div id="team" class="team-members">
                                             <p>You do not belong to a team yet</p>
                                             <button class="btn btn-outline-primary" id="create_team" data-toggle="modal"
                                                     data-target="#createTeamModal">Create Team
                                             </button>
                                             <button class="btn btn-outline-primary" id="join_team" data-toggle="modal"
                                                     data-target="#joinTeamModal">Join Team
                                             </button>

                                         </div>

                                     @elseif($team->num_members <= 1)
                                         <div id="team" class="team-members">
                                             <p>You currently don't have other members in your team</p>
                                             <butss="btn btn-outline-primary" id="invite_team" data-toggle="modal"
                                                     data-target="#createTeamModal">Invite members
                                             </button>


                                         </div>
                                     @endif


                                     {{--

                                     <div class="team-members">
                                         <h4>Team members</h4>

                                         <div class="row">
                                             <div class="col-lg-3 col-md-4 col-sm-6">
                                                 <img src="/images/users/dummy.jpeg" alt="" class="img-fluid rounded">
                                                 <h4>Matthew Collins</h4>
                                                 <p> Head of corporate affairs</p>
                                             </div>

                                             <div class="col-lg-3 col-md-4 col-sm-6">
                                                 <img src="/images/users/dummy.jpeg" alt="" class="img-fluid rounded">
                                                 <h4>Matthew Collins</h4>
                                                 <p> Head of corporate affairs</p>
                                             </div>


                                             <div class="col-lg-3 col-md-4 col-sm-6">
                                                 <img src="/images/users/dummy.jpeg" alt="" class="img-fluid rounded">
                                                 <h4>Matthew Collins</h4>
                                                 <p> Head of corporate affairs</p>
                                             </div>


                                             <div class="col-lg-3 col-md-4 col-sm-6">
                                                 <img src="/images/users/dummy.jpeg" alt="" class="img-fluid rounded">
                                                 <h4>Matthew Collins</h4>
                                                 <p> Head of corporate affairs</p>
                                             </div>


                                         </div>


                                     </div>


                                     --}}

                </div>


            </div>


        </div>
    </div>
@endsection

@section('extraScript')
    <script src="/js/cropper.min.js"></script>
    <script src="/js/profile.js"></script>
@stop