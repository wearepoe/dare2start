@extends('userlayouts.master')

@section('extraStyles')
    <link rel="stylesheet" href="/css/cropper.css">
@stop


@section('content')

    <div class="container d2s-detail-container">
        <div class="row">
            <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"> <i class="fa fa-arrow-left"></i> Back to profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#basic-information">Basic Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about-me">About Me</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#social-media-handles">Social Media Handles</a>
                    </li>
                </ul>

            </nav>

            <div class="col-xs-12 col-md-10 _editor-content_">
                <div class="affix-block" id="basic-information">
                    <div class="d2s-large-post">
                        <div class="info-block style-2">
                            <div class="d2s-large-post-align"><h3 class="info-block-label">Basic Information</h3></div>
                        </div>
                        <div class="d2s-large-post-align">
                            <form id="bio-edit-form" class="dropzone">
                                {{csrf_field()}}

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <div class="upload-dropzone">
                                                <div><i class="fa fa-plus-square-o"></i> Logo</div>
                                                <input type="file" name="team_logo" id="team_logo"
                                                       accept="image/x-png,image/jpeg,image/jpg">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label for="team-name" class="form-label">Team name</label>
                                            <input type="text" class="form-input" id="team-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="bus-category" class="form-label">Business category</label>

                                            <select id="bus-category" class="form-input">
                                                <option value="agriculture">Agriculture</option>
                                                <option value="agriculture">Agriculture</option>
                                                <option value="agriculture">Agriculture</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="team-bio" class="form-label">Business Description</label>
                                            <textarea class="form-control form-input" id="team-bio" rows="3"></textarea>
                                            <span class="help-block float-right"
                                                  style="color: #cdcdcd">400 characters</span>
                                        </div>
                                    </div>

                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('extraScript')
    <script src="/js/cropper.min.js"></script>
@stop



