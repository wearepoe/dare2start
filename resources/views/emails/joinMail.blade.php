@component('mail::message')
Hello {{$receiversName}},

{{$sendersName}} has sent an invite to join your team

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
