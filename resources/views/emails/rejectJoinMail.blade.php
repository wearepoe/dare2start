@component('mail::message')
Hello {{$sendersName}},

Your request to join {{$teamName}}was not approved.

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
