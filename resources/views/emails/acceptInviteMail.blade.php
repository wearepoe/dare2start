@component('mail::message')
Hello {{$sendersName}},

Your invitation to {{$receiversName}} has been accepted.He/She is now officially a member of {{$teamName}}
@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
