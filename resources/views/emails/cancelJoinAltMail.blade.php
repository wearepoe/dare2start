@component('mail::message')
Hello {{$receiversName}},

{{$sendersName}} canceled his/her request to join your team

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
