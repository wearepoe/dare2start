@component('mail::message')
Hello {{$sendersName}},

You canceled your request to join {{$teamName}}

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
