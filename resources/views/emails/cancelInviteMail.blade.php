@component('mail::message')

Hello {{$sendersName}}

You canceled your team invitation to {{$receiversName}}
@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
