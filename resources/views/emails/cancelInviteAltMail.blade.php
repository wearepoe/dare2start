@component('mail::message')
Hello {{$receiversName}}

{{$sendersName}} canceled his/her invite for you to join {{$teamName}}

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
