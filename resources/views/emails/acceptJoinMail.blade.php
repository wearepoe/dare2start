@component('mail::message')
Hello {{$sendersName}}

Your request to join <b>{{$teamName}}</b> has been approved.
You are now officially a member of {{$teamName}}

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
