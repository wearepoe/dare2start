@component('mail::message')
Hello {{$receiversName}},

You have been invited to join {{$teamName}} by {{$sendersName}}

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
