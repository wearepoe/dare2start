@component('mail::message')
Hello {{$sendersName}},

Your invitation to {{$receiversName}} was not accepted.

@component('mail::button', ['url' => 'https://dare2start.com/profile'])
View your profile
@endcomponent
@endcomponent
