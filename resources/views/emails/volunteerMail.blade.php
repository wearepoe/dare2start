@component('mail::message')
Hii <b>{{$name}}</b>,

Thank you for expressing your wish to help make dare2start a success.
We will contact you as soon as possible to let you know the next action to take
on your request to volunteer

{{ config('app.name') }}
@endcomponent
