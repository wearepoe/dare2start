<?php

namespace App\Http\Controllers;

use App\Mail\RequestsMail;
use App\Notifications\UserRequestsNotification;
use App\Team;
use App\UsersProfile;
use Illuminate\Http\Request;
use Auth;
use App\UserRequest;
use Illuminate\Support\Facades\Mail;

class UserRequestsController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function sendRequest() {
		$this->validate( request(), [
			'type' => 'required',
			'to'   => 'required|integer',
		] );


		$response = '';

		switch ( request()->type ) {
			case "bio":
				//do something

				$message  = "Requested to add your bio";
				$response = $this->send( 'bio', request()->to, $message );

				return $response;
				break;

			case "team-role":
				//do something
				$message  = "Request to add your team position";
				$response = $this->send( 'team-position', request()->to, $message );

				return $response;
				break;

			case "join-team":
				//do something
				$message  = "Has requested to join your team";
				$response = $this->send( 'join-team', request()->to, $message );

				return $response;
				break;

			case "invite-to-join":
				//do something
				$message  = "has invited you to join their team";
				$response = $this->send( 'invite-to-join', request()->to, $message );

				return $response;
				break;

			default:
				return "something is not right";

		}

	}

	private function send( $type, $to, $message, $saveRequest = false ) {
		$userProfile     = Auth::user()->profile;
		$receiverProfile = UsersProfile::find( $to );


		if ( count( UserRequest::where( 'sent_by', '=', $userProfile->id )->where( 'received_by', '=', $to )->get() ) == 1 ) {

			return response()->json( [ "error" => "You have a pending request for team position" ] );

		} else {

			if ( $saveRequest == true ) {
				$userRequest = UserRequest::create( [
					'sent_by'     => $userProfile->id,
					'received_by' => $to,
					'type'        => $type,
					'message'     => $message,
				] );
			} else {
				$userRequest = null;
			}


			$items = [
				'sender_name'       => $userProfile->fullname,
				'sender_profile_id' => $userProfile->id,
				'message'           => $message,
				'notification_type' => $type,
				'received_by'       => $to,
			];

			$receiverProfile->notify( new UserRequestsNotification( $items ) );


			return $userRequest;
		}


	}

	public function readNotifications() {
		Auth::user()->profile->unreadNotifications->markAsRead();
		$response = [ 'unreadNotifications' => 0 ];

		return response()->json( $response );
	}


	public function joinTeam() {
		$this->validate( request(), [
			'team_id' => 'required|integer',
		] );

		$user         = Auth::user();
		$user_profile = $user->profile;

		if ( $user_profile->team_id == 0 && $user_profile->request_status == 'free' ) {

			$team      = Team::find( request()->team_id );
			$teamOwner = UsersProfile::find($team->users_profile_id);
			if ( ! $team == [] ) {

				$message                      = "Has requested to join your team";
				$response                     = $this->send( 'join-team', $team->users_profile_id, $message, true );
				$user_profile->request_status = "pending";
				$user_profile->save();


				Mail::to( $teamOwner->email )->send( new RequestsMail( $user_profile, $teamOwner, $team, "join" ) );


				//return view
				return response()->json( [ 'message' => 'Your request has been sent for approval' ] );

			} else {
				//return team doesn't exist
				return response( "team doesn't exist", 442 );
			}


		} else {
			return response( "user already belongs to a team or has a pending request to join a team", 442 );
		}


	}

	public function inviteMember() {
		$this->validate( request(), [
			'member_id' => 'required|integer',
		] );

		$user               = Auth::user();
		$user_profile       = $user->profile;
		$team               = $user_profile->team;
		$invitedUserProfile = UsersProfile::find( request()->member_id );

		if ( ! $team == [] ) {

			if ( $team->num_members < 5 && $invitedUserProfile->request_status == 'free' ) {
				//go ahead
				$message                            = "An invite to join a team has been sent";
				$response                           = $this->send( 'invite-to-join', $invitedUserProfile->id, $message, true );
				$invitedUserProfile->request_status = "pending";
				$invitedUserProfile->save();


				Mail::to( $invitedUserProfile->email )->send( new RequestsMail( $user_profile, $invitedUserProfile, $user_profile->team, "invite" ) );


				//return view
				return response()->json( [
					'message' => 'Your invitation has been sent for approval',
					'refresh' => true
				] );

			} else {
				return response( "your team is full or the person you are trying to invite has a pending invite", 442 );

			}


		} else {
			//return team doesn't exist
			return response( "team doesn't exist", 442 );
		}


	}

}
