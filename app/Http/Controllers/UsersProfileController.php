<?php

namespace App\Http\Controllers;

use App\UserRequest;
use App\UsersProfile;
use Auth;
use Illuminate\Http\Request;
use File;
use Illuminate\Notifications\Notifiable;
use Response;

class UsersProfileController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index() {
		$user      = Auth::user();
		$user_info = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$team     = $user_info->team;
		$requests = UserRequest::where( 'received_by', $user_info->id )->orWhere( 'sent_by', $user_info->id )->get();


		return view( 'profile.home', compact( 'user_info', 'team', 'requests' ) );
	}

	public function updatePicture() {

		//dd( request() );
		$this->validate( request(),
			[
				'profile_picture' => 'required|image|max:10000',
			] );


		$user    = Auth::user();
		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		//get the previous image


		if ( $profile->image != "default.jpg" ) {
			//if the profile image is not default.jpg
			//delete the old image
			//File::delete( public_path() . '/images/users/' . $profile->image );
			File::delete(  '/home/darefpki/public_html/images/users/' . $profile->image );

		}

		//upload the image here
		if ( request()->profile_picture->getClientOriginalExtension() == "" ) {
			$newImageName = time() . '.png';

		} else {
			$newImageName = time() . '.' . request()->profile_picture->getClientOriginalExtension();
		}

		//request()->profile_picture->move( public_path( 'images/users' ), $newImageName );
		request()->profile_picture->move('/home/darefpki/public_html/images/users/', $newImageName );


		$profile->image = $newImageName;
		$profile->save();


		$response = [
			'message'    => 'profile image change successful',
			'image_name' => $newImageName,
		];

		return response()->json( json_encode( $response ) );

		//return view( 'profile.joinTeam' );


	}

	public function updateInformation() {

		$this->validate( request(), [
			'fullname'        => 'required|between:3,255|regex:/^[\pL\s\-]+$/u',
			'gender'          => 'required|alpha',
			'birthday'        => 'required',
			'level'           => 'required|numeric',
			'department'      => 'required|between:3,255',
			'hallOfResidence' => 'required|between:3,255',
		] );


		$user = Auth::user();

		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$profile->fullname      = request()->fullname;
		$profile->gender        = request()->gender;
		$profile->date_of_birth = request()->birthday;
		$profile->level         = request()->level;
		$profile->department    = request()->department;
		$profile->hall          = request()->hallOfResidence;
		$profile->save();


		$response = [
			'message' => 'update successful',
			'result'  => $profile->bio
		];

		return response()->json( json_encode( $response ) );
	}

	public function updateExtra() {

		$this->validate( request(), [
			'phone_number'           => 'nullable|numeric|digits_between:10,15',
			'additional_email'       => 'nullable|email',
			'address'                => 'nullable',
			'favourite_entrepreneur' => 'nullable',
		] );


		$user = Auth::user();

		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$profile->phone_number           = request()->phone_number;
		$profile->additional_email       = request()->additional_email;
		$profile->address                = request()->address;
		$profile->favourite_entrepreneur = request()->favourite_entrepreneur;
		$profile->save();


		$response = [
			'message' => 'update successful',
		];

		return response()->json( json_encode( $response ) );
	}

	public function updateBio() {

		$this->validate( request(),
			[
				'bio' => 'required|min:3|max:400'
			] );


		$user = Auth::user();

		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$profile->bio = request()->bio;
		$profile->save();


		$response = [
			'message' => 'update successful',
			'result'  => $profile->bio
		];

		return response()->json( json_encode( $response ) );

		//return view( 'profile.joinTeam' );


	}

	public function updateRole() {

		$this->validate( request(),
			[
				'team_role' => 'required|min:3|max:400'
			] );


		$user = Auth::user();

		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$profile->team_role = request()->team_role;
		$profile->save();


		$response = [
			'message' => 'update successful',
			'result'  => $profile->team_role
		];

		return response()->json( json_encode( $response ) );

		//return view( 'profile.joinTeam' );


	}

	public function updateSocial() {

		$this->validate( request(), [
			'facebook'  => 'nullable|min:3',
			'twitter'   => 'nullable|min:3',
			'instagram' => 'nullable|min:3',
			'linkedin'  => 'nullable|min:3',
		] );

		$facebookSearchArray = [
			"https://www.facebook.com/",
			"http://www.facebook.com/",
			"www.facebook.com/",
			"facebook.com/"
		];
		$facebook            = str_replace( $facebookSearchArray, "", request()->facebook );


		$twitterSearchArray = [
			"https://www.twitter.com/",
			"http://www.twitter.com/",
			"www.twitter.com/",
			"twitter.com/"
		];
		$twitter            = str_replace( $twitterSearchArray, "", request()->twitter );

		$instagramSearchArray = [
			"https://www.instagram.com/",
			"http://www.instagram.com/",
			"www.instagram.com/",
			"instagram.com/"
		];
		$instagram            = str_replace( $instagramSearchArray, "", request()->instagram );

		$linkedinSearchArray = [
			"https://www.linkedin.com/",
			"http://www.linkedin.com/",
			"www.linkedin.com/",
			"linkedin.com/"
		];
		$linkedin            = str_replace( $linkedinSearchArray, "", request()->linkedin );


		$user = Auth::user();

		$profile = UsersProfile::where( 'user_id', "=", $user->id )->first();

		$profile->facebook  = $facebook;
		$profile->instagram = $instagram;
		$profile->twitter   = $twitter;
		$profile->linkedin  = $linkedin;
		$profile->save();


		$response = [
			'message' => 'update successful'
		];

		return response()->json( json_encode( $response ) );
	}

	public function editUser() {
		$user      = Auth::user();
		$user_info = UsersProfile::where( 'user_id', "=", $user->id )->first();
		$team      = $user_info->team;

		return view( 'profile.edituser', compact( 'user_info', 'team' ) );
	}

	//create team
	public function createTeam() {
		$user      = Auth::user();
		$user_info = UsersProfile::where( 'user_id', "=", $user->id )->first();
		$team      = $user_info->team;

		return view( 'team.createTeam', compact( 'user_info', 'team' ) );
	}


}
