<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\PageOptions;

class BlogController extends Controller
{
    //
    public function index()
    {
        $page_options = PageOptions::first();
        $blog_posts = Blog::latest()->get();
        return view('pages.blog', compact('page_options', 'blog_posts'));
    }

    public function show(Blog $blog_post)
    {
        $page_options = PageOptions::first();

        return view('pages.singlepost', compact('page_options', 'blog_post'));
    }
}
