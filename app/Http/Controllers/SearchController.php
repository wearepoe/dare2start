<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UsersProfile;
use App\Team;

class SearchController extends Controller {
	//
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function searchFor() {
		$searchItem = request()->searchFor;
		$results    = UsersProfile::search( $searchItem )->get();

		$view = view( 'profile.searchItem', compact( 'results' ) )->render();
		return response()->json( [ 'html' => $view ] );


	}

	public function searchForTeam() {
		$searchItem = request()->searchFor;
		$results    = Team::search( $searchItem )->get();

		$view = view( 'profile.searchTeamItem', compact( 'results' ) )->render();
		return response()->json( [ 'html' => $view ] );

	}

}
