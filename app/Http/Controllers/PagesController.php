<?php

namespace App\Http\Controllers;

use App\Blog;
use App\PageOptions;
use App\Sponsor;
use App\Message;
use App\Team;
use Illuminate\Http\Request;


class PagesController extends Controller {


	##############################################
	#              get requests                 #
	#############################################

	public function index() {
		$page_options = PageOptions::first();
		$sponsors     = Sponsor::all();
		$blog_posts   = Blog::latest()->skip( 0 )->take( 4 )->get();

		return view( 'pages.home', compact( 'sponsors', 'page_options', 'blog_posts' ) );
	}

	public function about() {
		$page_options = PageOptions::first();

		return view( 'pages.about', compact( 'page_options' ) );
	}

	public function memories() {
		$page_options = PageOptions::first();

		return view( 'pages.memories', compact( 'page_options' ) );
	}


	public function contact() {
		$page_options = PageOptions::first();

		return view( 'pages.contact', compact( 'page_options' ) );
	}


	public function faq() {
		$page_options = PageOptions::first();

		return view( 'pages.faq', compact( 'page_options' ) );
	}

	public function volunteer() {
		$page_options = PageOptions::first();

		return view( 'pages.volunteer', compact( 'page_options' ) );
	}

	public function teams() {
		$page_options = PageOptions::first();
		$teams = Team::paginate(1);
		return view( 'pages.allTeams', compact( 'teams' ,'page_options') );

	}













	##############################################
	#               login and sign up              #
	#############################################
	public function login() {
		$page_options = PageOptions::first();

		if ( ! is_null( $page_options ) && $page_options->reg_started ) {
			return view( 'pages.login', compact( 'page_options' ) );
		} else {
			$sponsors = Sponsor::all();

			return view( 'pages.home', compact( 'sponsors', 'page_options' ) );
		}
	}


}
