<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.adminLogin');
    }


    public function login(Request $request)
    {
        //validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'

        ]);

        // attempt to log the user in
        //when using auth don't forget the guard

        $credentials = ['email' => $request->email, 'password' => $request->password];
        $remember = request('remember');
        if (Auth::guard('admin')->attempt($credentials, $remember)) {
            return redirect()->intended('admin.dashboard');
        }


        //if successful then redirect to the intended location
        // if uncessful redirect back to the login
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('/');

    }
}
