<?php

namespace App\Http\Controllers\Auth;


use App\Mail\RegistrationComplete;
use Illuminate\Support\Facades\Mail;
use App\User;
use Avatar;
use App\UsersProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Symfony\Component\HttpKernel\Profiler\Profile;

class RegisterController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/profile';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'guest' );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator( array $data ) {
		return Validator::make( $data, [
			'name'     => 'required|string|max:255',
			'email'    => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		] );
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 *
	 * @return User
	 */
	protected function create( array $data ) {
		$user = User::create( [
			'name'     => $data['name'],
			'email'    => $data['email'],
			'password' => bcrypt( $data['password'] ),
		] );


		$newImageName = time() . '.png';
		//Avatar::create( $user->name )->save( '/home/darefpki/public_html/images/users/' . $newImageName, 100 );
		Avatar::create( $user->name )->save( public_path( 'images/users/' ) . $newImageName, 100 );

		$user->profile()->save( UsersProfile::create( [
			'user_id'  => $user->id,
			'fullname' => $user->name,
			'email'    => $user->email,
			'image'    => $newImageName,
		] ) );

		Mail::to($user->email)
		    ->send(new RegistrationComplete($user->profile));

		return $user;
	}
}
