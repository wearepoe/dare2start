<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\VolunteerRequest;
use App\Volunteer;
use Illuminate\Http\Request;
use App\PageOptions;

class VolunteerController extends Controller {
	//


	public function show() {
		$page_options = PageOptions::first();

		return view( 'pages.volunteer', compact( 'page_options' ) );
	}


	public function addVolunteer() {
		$this->validate( request(), [
			'fullname'     => 'required|between:3,255|regex:/^[\pL\s\-]+$/u',
			'email'        => 'required|email|unique:volunteers',
			'phone_number' => 'required|numeric',
			'section'      => 'required',
			'other_info'   => 'nullable',

		] );

		$volunteer = Volunteer::create( [
			'fullname'     => request()->fullname,
			'email'        => request()->email,
			'phone_number' => request()->phone_number,
			'section'      => request()->section,
			'other_info'   => request()->other_info,
		] );

		Mail::to( $volunteer->email )->send( new VolunteerRequest( $volunteer ) );

		return redirect( '/volunteer/thanks' );

	}

	public function thankYou() {

		$page_options = PageOptions::first();

		return view( 'pages.thankYou', compact( 'page_options' ) );
	}
}
