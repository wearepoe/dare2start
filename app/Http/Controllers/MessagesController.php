<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\PageOptions;

class MessagesController extends Controller
{
    //
    ##############################################
    #               post requests               #
    #############################################

    // "mfws" means message from website
    public function mfws()
    {

        $this->validate(request(),
            [
                'full_name' => 'required|min:3|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email|min:3',
                'phone_number' => 'sometimes|nullable|numeric|digits_between:10,15',
                'message' => 'required|min:3',
            ]);


        $mfws = new Message;

        $message_body = "Name:" . ucwords(request("full_name")) . "\n" . "Email:" . request("email") . "\n" . "Phone #:" . request("phone_number") . "\n" . request("message");

        $mfws->title = "Message from Website";
        $mfws->body = $message_body;
        $mfws->sent_by = request("email");
        $mfws->received_by = "admin";

        //save the message
        $mfws->save();

        $page_options = PageOptions::first();
        return back()->with('message', 'Message Sent');
    }



}
