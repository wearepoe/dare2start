<?php

namespace App\Http\Controllers;

use App\Mail\RequestsMail;
use App\Notifications\UserRequestsNotification;
use App\UserRequest;
use App\UsersProfile;
use Auth;
use Avatar;
use Illuminate\Http\Request;
use App\Team;
use File;
use Illuminate\Support\Facades\Mail;

class TeamController extends Controller {
	//
	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function create() {

		$user      = Auth::user();
		$user_info = UsersProfile::where( 'user_id', "=", $user->id )->first();
		$team      = $user_info->team;

		return view( 'team.createTeam', compact( 'user_info', 'team' ) );
	}

	public function createTeam() {
		$this->validate( request(),
			[
				'team_name'   => 'required|min:3|max:255',
				'category'    => 'required',
				'description' => 'required',
			] );

		$user    = Auth::user();
		$profile = $user->profile;
		$team    = $profile->team;


		if ( $team == [] ) {

			//remove the request row
			$usersRequests = UserRequest::where( 'sent_by', $user->profile->id )->orWhere( 'received_by', $user->profile->id );
			$usersRequests->delete();

			$newImageName = time() . '.png';
			//Avatar::create( request()->team_name )->save( public_path( 'images/teamlogos/' ) . $newImageName, 100 );
			Avatar::create( request()->team_name )->save( '/home/darefpki/public_html/images/teamlogos/' . $newImageName, 100 );

			//request()->team_logo->move( public_path( 'images/teamlogos' ), $newImageName );

			$team = Team::create( [
				'name'             => request()->team_name,
				'slug'             => str_replace( " ", '-', strtolower( request()->team_name ) ),
				'category'         => request()->category,
				'bio'              => request()->description,
				'user_id'          => $user->id,
				'users_profile_id' => $user->profile->id,
				'image'            => $newImageName,
			] );

			$userProfile          = $user->profile;
			$userProfile->team_id = $team->id;
			$userProfile->save();

			$response = [
				'message' => 'team created successfully',
				'slug'    => $team->slug,
			];

			return response()->json( json_encode( $response ) );
		} else {
			return response( 'This user already belongs to a team', 422 );
		}


	}

	public function show( $slug ) {
		$team = Team::where( 'id', $slug )->orWhere( 'slug', $slug )->firstOrFail();

		return view( 'team.teamProfile', compact( 'team' ) );

	}

	public function edit( $slug ) {
		$user     = Auth::user();
		$profile  = $user->profile;
		$userTeam = $profile->team;

		$team = Team::where( 'id', $slug )->orWhere( 'slug', $slug )->firstOrFail();

		if ( $userTeam == [] || ! $team->id == $profile->team_id ) {
			return redirect( '/profile' );
		} else {
			$team = Team::where( 'id', $slug )->orWhere( 'slug', $slug )->firstOrFail();

			return view( 'team.editTeam', compact( 'team' ) );
		}


	}

	public function invite() {
		$this->validate( request(), [
			'member_id' => 'required|numeric'
		] );

		$user = Auth::user();

		$currentUserTeam = $user->profile->team;

		$userProfileToInvite = UsersProfile::find( request()->member_id );

		if ( $currentUserTeam->status == "available" && $currentUserTeam->num_members < 5 ) {
			if ( $userProfileToInvite->team_id == 0 ) {
				//go ahead
				//changing the team id
				$userProfileToInvite->team_id = $currentUserTeam->id;
				$userProfileToInvite->save();

				$currentUserTeam->num_members = ( $currentUserTeam->num_members ) + 1;

				if ( $currentUserTeam->num_members == 5 ) {
					$currentUserTeam->status = "full";
				}
				$currentUserTeam->save();


				$view = view( 'profile.membersItem', compact( 'userProfileToInvite' ) )->render();

				return response()->json( [
					'html'             => $view,
					'num_members'      => $currentUserTeam->num_members,
					'num_members_left' => $currentUserTeam->num_members . " " . str_plural( 'Member', $currentUserTeam->num_members ),
					'team_name'        => $currentUserTeam->name,
				] );

			} else {
				// reject
				return response( "The person you are trying to invite already belongs to a team", 442 );
			}
		} else {
			return response( "The team is full", 442 );

		}


	}

	public function join() {
		$this->validate( request(), [
			'team_id' => 'required|numeric'
		] );

		$user         = Auth::user();
		$user_profile = $user->profile;

		if ( $user_profile->team_id == 0 ) {

			$team = Team::find( request()->team_id );

			if ( ! $team == [] ) {


				if ( $team->status == "available" && $team->num_members < 5 ) {
					$user_profile->team_id = $team->id;
					$user_profile->save();
					//update the team count
					$team->num_members = $team->num_members + 1;
					$team->save();

					//return view
					return response()->json( [ 'refresh' => true ] );
				}


			} else {
				//return team doesn't exist
				return response( "team doesn't exist", 442 );
			}


		} else {
			//return user belongs to team
			return response( "user already belongs to a team", 442 );

		}


	}

	public function approveJoin() {
		// send approval request

		$this->validate( request(), [
			'senders_id'   => 'required|numeric',
			'receivers_id' => 'required|numeric',
			'request_id'   => 'required|numeric'
		] );

		$user = Auth::user();

		$receiversProfile = $user->profile;
		$currentUserTeam  = $user->profile->team;

		$sendersProfile = UsersProfile::find( request()->senders_id );

		if ( $currentUserTeam->users_profile_id == $user->profile->id ) {

			if ( $sendersProfile->team_id == 0 ) {
				//go ahead
				//changing the team id
				$sendersProfile->team_id        = $currentUserTeam->id;
				$sendersProfile->request_status = 'free';
				$sendersProfile->save();

				Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $receiversProfile->team, "approve-join" ) );

				//changing the count of the number of members
				if ( $currentUserTeam->num_members < 5 && $currentUserTeam->status == "available" ) {
					$currentUserTeam->num_members = ( $currentUserTeam->num_members ) + 1;

					if ( $currentUserTeam->num_members == 5 ) {
						$currentUserTeam->status = "full";
					}

					$currentUserTeam->save();
				} else {
					return response( "The team is full", 442 );

				}

				//remove the request row
				$usersRequests = UserRequest::find( request()->request_id );
				$usersRequests->delete();

				// send a notification

				$items = [
					'sender_name'       => $user->profile->fullname,
					'sender_profile_id' => $user->profile->id,
					'message'           => "has accepted your request to join his/her team",
					'notification_type' => 'approved-team-join',
					'received_by'       => $sendersProfile->id,
				];

				$sendersProfile->notify( new UserRequestsNotification( $items ) );

				$userProfileToInvite = $sendersProfile;


				$view = view( 'profile.membersItem', compact( 'userProfileToInvite' ) )->render();

				return response()->json( [
					'html'             => $view,
					'num_members'      => $currentUserTeam->num_members,
					'num_members_left' => $currentUserTeam->num_members . " " . str_plural( 'Member', $currentUserTeam->num_members ),
					'team_name'        => $currentUserTeam->name,
					'request_count'    => $usersRequests->count(),

				] );

			} else {
				// reject
				return response( "The person you are trying to approve already belongs to a team", 442 );
			}
		} else {
			return response( "Only the team leader can approve a team", 442 );
		}


	}

	public function rejectJoin() {
		// send approval request

		$this->validate( request(), [
			'senders_id' => 'required|numeric',
			'request_id' => 'required|numeric'
		] );

		$user = Auth::user();

		$receiversProfile = $user->profile;

		$currentUserTeam = $user->profile->team;

		$sendersProfile = UsersProfile::find( request()->senders_id );

		if ( $currentUserTeam->users_profile_id == $user->profile->id ) {

			if ( $sendersProfile->team_id == 0 ) {

				$sendersProfile->request_status = 'free';
				$sendersProfile->save();


				//remove the request row
				$usersRequests = UserRequest::find( request()->request_id );
				$usersRequests->delete();


				//send email
				Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $currentUserTeam, "reject-join" ) );


				// send a notification

				$items = [
					'sender_name'       => $user->profile->fullname,
					'sender_profile_id' => $user->profile->id,
					'message'           => "did not accept your request to join his/her team",
					'notification_type' => 'rejected-team-join',
					'received_by'       => $sendersProfile->id,
				];

				$sendersProfile->notify( new UserRequestsNotification( $items ) );

				return response()->json( [
					'reject'        => 'successful',
					'request_count' => $usersRequests->count(),
				] );

			} else {
				// reject
				return response( "The person you are trying to reject already belongs to a team", 442 );
			}
		} else {
			return response( "Only the team leader can reject a team member", 442 );
		}


	}

	public function cancelJoin() {
		// send approval request

		$this->validate( request(), [
			'senders_id'   => 'required|numeric',
			'receivers_id' => 'required|numeric',
			'request_id'   => 'required|numeric'
		] );

		$user = Auth::user();

		$receiversProfile = UsersProfile::find( request()->receivers_id );
		$currentUserTeam  = $user->profile->team;

		$sendersProfile = UsersProfile::find( request()->senders_id );

		if ( $sendersProfile->team_id == 0 ) {

			$sendersProfile->request_status = 'free';
			$sendersProfile->save();

			//remove the request row
			$usersRequests = UserRequest::find( request()->request_id );
			$usersRequests->delete();

			//send email
			Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $receiversProfile->team, "cancel-join" ) );
			//to the receiver
			Mail::to( $receiversProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $receiversProfile->team, "cancel-join-alt" ) );


			return response()->json( [
				'cancel'        => 'successful',
				'request_count' => $usersRequests->count(),
			] );

		} else {
			// reject
			return response( "The request to cancel failed ", 442 );
		}


	}

	public function acceptInvite() {
		// send approval request

		$this->validate( request(), [
			'senders_id'   => 'required|numeric',
			'receivers_id' => 'required|numeric',
			'request_id'   => 'required|numeric'
		] );

		$user = Auth::user();


		$receiversProfile = UsersProfile::find( request()->receivers_id );
		$sendersProfile   = UsersProfile::find( request()->senders_id );

		$team = $sendersProfile->team;

		$usersRequests = UserRequest::find( request()->request_id );

		if ( $usersRequests->received_by == $user->profile->id ) {

			if ( $receiversProfile->team_id == 0 ) {
				//go ahead
				//changing the team id
				$receiversProfile->team_id        = $team->id;
				$receiversProfile->request_status = 'free';;
				$receiversProfile->save();


				//changing the count of the number of members
				if ( $team->num_members < 5 && $team->status == "available" ) {
					$team->num_members = ( $team->num_members ) + 1;

					if ( $team->num_members == 5 ) {
						$team->status = "full";
					}

					$team->save();
				} else {
					return response( "The team is full", 442 );

				}

				//remove the request row


				$usersRequests->delete();

				//send email
				Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $sendersProfile->team, "accept-invite" ) );


				// send a notification

				$items = [
					'sender_name'       => $user->profile->fullname,
					'sender_profile_id' => $user->profile->id,
					'message'           => "has accepted your request to join his/her team",
					'notification_type' => 'approved-team-join',
					'received_by'       => $receiversProfile->id,
				];

				//$sendersProfile->notify( new UserRequestsNotification( $items ) );

				return response()->json( [
					'acceptance'    => 'successful',
					'refresh'       => true,
					'request_count' => $usersRequests->count(),

				] );

			} else {
				// reject
				return response( "You already belong to a team", 442 );
			}
		} else {
			return response( "Something went wrong", 442 );
		}


	}

	public function rejectInvite() {
		// send approval request

		$this->validate( request(), [
			'receivers_id' => 'required|numeric',
			'senders_id'   => 'required|numeric',
			'request_id'   => 'required|numeric'
		] );

		$user = Auth::user();

		$currentUserTeam = $user->profile->team;

		$receiversProfile = UsersProfile::find( request()->receivers_id );
		$sendersProfile   = UsersProfile::find( request()->senders_id );

		$usersRequests = UserRequest::find( request()->request_id );


		if ( $usersRequests->received_by == $user->profile->id ) {

			if ( $receiversProfile->team_id == 0 ) {

				$receiversProfile->request_status = 'free';
				$receiversProfile->save();

				//remove the request row
				$usersRequests = UserRequest::find( request()->request_id );

				$usersRequests->delete();

				Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $sendersProfile->team, "reject-invite" ) );


				// send a notification

				$items = [
					'sender_name'       => $user->profile->fullname,
					'sender_profile_id' => $user->profile->id,
					'message'           => "did not accept the request to join your team",
					'notification_type' => 'rejected-invite',
					'received_by'       => $usersRequests->sent_by,
				];

				//$sendersProfile->notify( new UserRequestsNotification( $items ) );

				return response()->json( [
					'reject'        => 'successful',
					'request_count' => $usersRequests->count(),
				] );

			} else {
				// reject
				return response( "You already belong to a team", 442 );
			}
		} else {
			return response( "Something went wrong", 442 );
		}


	}

	public function cancelInvite() {
		// send approval request

		$this->validate( request(), [
			'receiver_id' => 'required|numeric',
			'request_id'  => 'required|numeric'
		] );

		$user = Auth::user();

		$currentUserTeam = $user->profile->team;

		$sendersProfile = $user->profile;

		$receiversProfile = UsersProfile::find( request()->receiver_id );

		if ( $receiversProfile->team_id == 0 ) {

			$receiversProfile->request_status = 'free';
			$receiversProfile->save();

			//remove the request row
			$usersRequests = UserRequest::find( request()->request_id );
			$usersRequests->delete();


			Mail::to( $sendersProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $sendersProfile->team, "cancel-invite" ) );
			Mail::to( $receiversProfile->email )->send( new RequestsMail( $sendersProfile, $receiversProfile, $sendersProfile->team, "cancel-invite-alt" ) );


			return response()->json( [
				'cancel'        => 'successful',
				'request_count' => $usersRequests->count(),
			] );

		} else {
			// reject
			return response( "The request to cancel failed ", 442 );
		}


	}


	public
	function leave() {
		$this->validate( request(), [
			'team_id' => 'required|numeric'
		] );

		$user         = Auth::user();
		$user_profile = $user->profile;

		if ( $user_profile->team_id > 0 ) {

			$team = Team::find( request()->team_id );

			if ( ! $team == [] ) {
				$user_profile->team_id        = 0;
				$user_profile->request_status = 'free';
				$user_profile->save();
				//update the team count

				if ( $team->num_members == 5 ) {
					$team->num_members = $team->num_members - 1;
					$team->status      = "available";
					$team->save();
				} else {
					$team->num_members = $team->num_members - 1;
					$team->save();
				}

				//return view
				return response()->json( [ 'refresh' => true ] );

			} else {
				//return team doesn't exist
				return response( "team doesn't exist", 442 );
			}


		} else {
			//return user belongs to team
			return response( "user does not belong to a team", 442 );

		}


	}

	public
	function remove() {
		$this->validate( request(), [
			'member_id' => 'required|numeric'
		] );

		$user   = Auth::user();
		$member = UsersProfile::find( request()->member_id );
		$team   = Team::find( $member->team_id );

		if ( $team->users_profile_id == $user->profile->id ) {

			$member->team_id        = 0;
			$member->request_status = 'free';
			if ( $team->num_members == 5 ) {
				$team->status = "available";
			}
			$team->num_members = $team->num_members - 1;
			$member->save();
			$team->save();

			return response()->json( [
				'num_members'      => $team->num_members,
				'num_members_left' => $team->num_members . " " . str_plural( 'Member', $team->num_members ),
				'member_id'        => $member->id,
				'status_message'   => $team->status
			] );

		} else {
			return response( "You are not the admin for this team", 442 );

		}


	}

	public
	function updateInformation() {
		$this->validate( request(), [
			'teamName'     => 'required|min:3|max:255',
			'teamIndustry' => 'required',
			'teamBio'      => 'required',
		] );


		$user = Auth::user();
		$team = $user->profile->team;


		if ( ! $team == [] ) {
			$team->name     = request()->teamName;
			$team->category = request()->teamIndustry;
			$team->bio      = request()->teamBio;
			$team->save();
			$response = [
				'message' => 'team updated successfully',
			];

			return response()->json( $response );
		} else {
			return response( "This user doesn't belong to a team", 422 );
		}
	}

	public
	function updateExtra() {

		/*
		 *   teamNumber: teamNumber.val(),
				teamEmail: teamEmail.val(),
				teamMission: teamMission.val(),
				teamVision: teamVision.val(),
				teamObjectives: teamObjectives.val()
		 */
		$this->validate( request(), [
			'teamNumber'     => 'nullable|numeric',
			'teamEmail'      => 'nullable|email',
			'teamMission'    => 'nullable',
			'teamVision'     => 'nullable',
			'teamObjectives' => 'nullable',


		] );


		$user = Auth::user();
		$team = $user->profile->team;


		if ( ! $team == [] ) {

			$team->phone_number = request()->teamNumber;
			$team->email        = request()->teamEmail;
			$team->mission      = request()->teamMission;
			$team->vision       = request()->teamVision;
			$team->objectives   = request()->teamObjectives;
			$team->save();
			$response = [
				'message' => 'team updated successfully',
			];

			return response()->json( $response );
		} else {
			return response( "This user doesn't belong to a team", 422 );
		}
	}

	public
	function updateSocial() {

		/*
		 *   teamNumber: teamNumber.val(),
				teamEmail: teamEmail.val(),
				teamMission: teamMission.val(),
				teamVision: teamVision.val(),
				teamObjectives: teamObjectives.val()
		 */
		$this->validate( request(), [
			'website'   => 'nullable',
			'facebook'  => 'nullable',
			'twitter'   => 'nullable',
			'instagram' => 'nullable',
			'linkedin'  => 'nullable',
		] );


		$user = Auth::user();
		$team = $user->profile->team;

		if ( ! $team == [] ) {

			$team->website   = request()->website;
			$team->facebook  = request()->facebook;
			$team->twitter   = request()->twitter;
			$team->instagram = request()->instagram;
			$team->linkedin  = request()->linkedin;
			if ( $team->save() ) {
				$response = [
					'message' => request()->website,
				];
			} else {
				$response = [
					'message' => 'something went wrong',
				];
			}


			return response()->json( $response );
		} else {
			return response( "This user doesn't belong to a team", 422 );
		}
	}

	public
	function changeLogo() {

		//dd( request() );
		$this->validate( request(),
			[
				'team_logo' => 'required|image|max:10000',
			] );


		$user = Auth::user();
		$team = $user->profile->team;

		//get the previous image


		if ( $team->image != "default.jpg" ) {
			//if the profile image is not default.jpg
			//delete the old image
			//File::delete( public_path() . '/images/teamlogos/' . $team->image );
			File::delete( '/home/darefpki/public_html/images/teamlogos/' . $team->image );

		}

		//upload the image here
		if ( request()->team_logo->getClientOriginalExtension() == "" ) {
			$newImageName = time() . '.png';

		} else {
			$newImageName = time() . '.' . request()->team_logo->getClientOriginalExtension();
		}

		//request()->team_logo->move( public_path( 'images/teamlogos/' ), $newImageName );
		request()->team_logo->move( '/home/darefpki/public_html/images/teamlogos/', $newImageName );


		$team->image = $newImageName;
		$team->save();


		$response = [
			'message'    => 'team logo change successful',
			'image_name' => $newImageName,
		];

		return response()->json( json_encode( $response ) );

		//return view( 'profile.joinTeam' );


	}

	public
	function deleteTeam() {
		$user    = Auth::user();
		$profile = $user->profile;
		$team    = $profile->team;

		if ( $profile->id == $team->users_profile_id ) {
			$team->delete();
			//remove the request row
			$usersRequests = UserRequest::where( 'sent_by', $user->profile->id )->orWhere( 'received_by', $profile->id );
			$usersRequests->delete();


			// i am using this because the usual one gives an mbrstring error
			$profileSame          = UsersProfile::find( $profile->id );
			$profileSame->team_id = 0;
			$profileSame->save();

			return redirect( '/profile' );
		} else {
			return redirect( '/profile' );
		}


	}


}
