<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;

class SubscriptionController extends Controller
{
    //
    public function store()
    {

        $customMessages = [
        'unique' => 'This email has already been subscribed'
        ];

        $this->validate(request(), [
            'sub-email' => 'required|email|unique:subscriptions,email'
            ], $customMessages);

        Subscription::create(['email' => request('sub-email')]);
        return back();
         
    }
}
