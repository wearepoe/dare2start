<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model {
	//

	protected $fillable = [
		'fullname',
		'email',
		'phone_number',
		'section',
		'other_info',
	];
}
