<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VolunteerRequest extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */

	protected $volunteer;

	public function __construct( $volunteer ) {
		//

		$this->volunteer = $volunteer;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->markdown( 'emails.volunteerMail' ) ->with([
			'name' => $this->volunteer->fullname
		]);
	}
}
