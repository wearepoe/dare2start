<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationComplete extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */

	protected $profile;

	public function __construct( $profile ) {
		//
		$this->profile = $profile;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->from( 'welcome@dare2start.com' )
		            ->view( 'emails.welcomeMail' )
		            ->with( [
			            'name' => $this->profile->fullname,
		            ] );
	}
}
