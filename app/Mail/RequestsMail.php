<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestsMail extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */

	protected $receiversProfile, $type, $sendersProfile, $teamInvolved;

	public function __construct( $sendersProfile, $receiversProfile, $teamInvolved, $type ) {
		//
		/* the senders profile refers to the person who initially sent the request to join the team
		the receivers profile on the other hand is the team leader who received the invite
		** note they are interchanged so don't get confused
		*/


		$this->sendersProfile   = $sendersProfile;
		$this->receiversProfile = $receiversProfile;
		$this->teamInvolved     = $teamInvolved;
		$this->type             = $type;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {

		switch ( $this->type ) {

			case ( "approve-join" ):
				return $this->markdown( 'emails.acceptJoinMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "reject-join" ):
				return $this->markdown( 'emails.rejectJoinMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "cancel-join" ):
				return $this->markdown( 'emails.cancelJoinMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "cancel-join-alt" ):
				return $this->markdown( 'emails.cancelJoinAltMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "accept-invite" ):
				return $this->markdown( 'emails.acceptInviteMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "reject-invite" ):
				return $this->markdown( 'emails.rejectInviteMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "cancel-invite" ):
				return $this->markdown( 'emails.cancelInviteMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;
			case ( "invite" ):
				return $this->markdown( 'emails.inviteMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;
			case ( "join" ):
				return $this->markdown( 'emails.joinMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			case ( "cancel-invite-alt" ):
				return $this->markdown( 'emails.cancelInviteAltMail' )->with( [
					'sendersName'   => $this->sendersProfile->fullname,
					'receiversName' => $this->receiversProfile->fullname,
					'teamName'      => $this->teamInvolved->name,
				] );
				break;

			default:
				return $this->markdown( 'emails.cancelJoinMail' )->with( [
					'sendersName'   => "TEST",
					'receiversName' => "TEST",
					'teamName'      => "TEST",
				] );
				break;


		}


	}
}
