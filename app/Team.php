<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


class Team extends Model {
	//a team belongs to many users
	use Searchable;

	protected $fillable = [
		'name',
		'slug',
		'category',
		'bio',
		'user_id',
		'users_profile_id',
		'image',

		'mission',
		'vision',
		'objectives',
		'phone_number',
		'email',
		'facebook',
		'twitter',
		'instagram',
		'linkedin',
		'website',


	];

	public function members() {
		return $this->hasMany( UsersProfile::class );
	}

	public function searchableAs() {
		return "teams_index";
	}
}
