<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;


class UsersProfile extends Model {
	//each profile belongs to a user

	use Searchable;
	use Notifiable;

	protected $fillable = [
		'user_id',
		'team_id',
		'fullname',
		'gender',
		'date_of_birth',
		'email',
		'phone_number',
		'additional-email',
		'address',
		'level',
		'department',
		'hall',
		'bio',
		'image',
		'team_role',
		'facebook',
		'twitter',
		'instagram',
		'linkedin',
	];

	public function user() {
		return $this->belongsTo( User::class );
	}

	public function team() {
		return $this->belongsTo( Team::class );
	}

	public function searchableAs() {
		return 'profiles_index';
	}


}
