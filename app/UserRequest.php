<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model {
	//

	protected $fillable = [
		'sent_by',
		'received_by',
		'type',
		'message',
	];
}
