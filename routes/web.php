<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


##############################################
#               get routes                  #
#############################################

//home page
Route::get( '/', 'PagesController@index' );

//about page
Route::get( '/about', 'PagesController@about' );

//memories page
Route::get( '/memories', 'PagesController@memories' );

//blog page
Route::get( '/blog', 'BlogController@index' );

//single blog post
Route::get( '/blog/{blog_post}', 'BlogController@show' );

//contact page
Route::get( '/contact', 'PagesController@contact' );

//faq page
Route::get( '/faq', 'PagesController@faq' );

//other routes
Route::get( '/volunteer', 'VolunteerController@show' );


//competition routes
Route::get( '/login', 'PagesController@login' );
Route::get( '/compete', 'PagesController@compete' );
Route::get( '/volunteer/thanks', 'VolunteerController@thankYou' );

//profile routes


##############################################
#              post routes                   #
#############################################


//post route for contact form
// "mfws" means message from website
Route::post( '/contact', 'MessagesController@mfws' );
//subscribe
Route::post( '/subscribe', 'SubscriptionController@store' );

Route::post( '/volunteer', 'VolunteerController@addVolunteer' );

Auth::routes();



##############################################
#              admin routes                 #
#############################################


Route::prefix( 'admin' )->group( function () {
// this will add a prefix to all the routes within the function
// its a shortcut

	Route::get( '/login', 'Auth\AdminLoginController@showLoginForm' )->name( 'admin.login' );

//post
	Route::post( '/login', 'Auth\AdminLoginController@login' )->name( 'admin.login.submit' );

//default to dashboard
	Route::get( '/', 'AdminsController@index' )->name( 'admin.dashboard' );

	Route::get( '/logout', 'Auth\AdminLoginController@logout' )->name( 'admin.logout' );
} );


##############################################
#              profile routes                 #
#############################################
Route::get( '/profile', 'UsersProfileController@index' )->name( 'profile' );
Route::post( '/team/create', 'TeamController@createTeam' );
Route::post( '/team/join', 'TeamController@joinTeam' );
Route::patch( '/bio', 'UsersProfileController@updateBio' );
Route::patch( '/update_bi', 'UsersProfileController@updateInformation' );
Route::patch( '/update_ei', 'UsersProfileController@updateExtra' );
Route::patch( '/update_social', 'UsersProfileController@updateSocial' );
Route::patch( '/update_role', 'UsersProfileController@updateRole' );
Route::post( '/update_picture', 'UsersProfileController@updatePicture' );
Route::get( '/profile/edit', 'UsersProfileController@editUser' );
Route::post( '/request/team_role', 'UserRequestsController@sendRequest' );
Route::get( '/markasread', 'UserRequestsController@readNotifications' );


##############################################
#              team routes                  #
#############################################
Route::get( '/teams', 'PagesController@teams' );
Route::get( '/team/create', 'TeamController@create' );
Route::get( '/team/{slug}', 'TeamController@show' );
Route::get( '/edit/team/{slug}', 'TeamController@edit' );
Route::get( '/team/edit/{slug}', 'TeamController@edit' );
Route::post( '/invite', 'UserRequestsController@inviteMember' );
Route::post( '/approve', 'TeamController@approveJoin' );
Route::post( '/reject', 'TeamController@rejectJoin' );
Route::post( '/cancel_join', 'TeamController@cancelJoin' );
Route::post( '/cancel_invite', 'TeamController@cancelInvite' );
Route::post( '/reject_invite', 'TeamController@rejectInvite' );
Route::post( '/accept_invite', 'TeamController@acceptInvite' );
Route::post( '/join', 'UserRequestsController@joinTeam' );
Route::post( '/remove', 'TeamController@remove' );
Route::post( '/leave', 'TeamController@leave' );
Route::patch( '/team/update_bi', 'TeamController@updateInformation' );
Route::patch( '/team/update_extra_i', 'TeamController@updateExtra' );
Route::patch( '/team/update_social', 'TeamController@updateSocial' );
Route::post( '/teamlogo', 'TeamController@changeLogo' );


//search routes
Route::post( '/search', 'SearchController@searchFor' );
Route::post( '/search_team', 'SearchController@searchForTeam' );

//users logout
Route::get( '/logout', 'Auth\LoginController@userLogout' )->name( 'user.logout' );