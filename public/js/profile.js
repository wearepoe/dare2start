$(document).ready(function () {
    $('#uploading-image').hide();
    var loaderPage = function () {
        $(".d2s-loader").fadeOut("slow");
    };

    loaderPage();

    //global file variable
    var file;


    $("#selectPicture").click(function (e) {
        e.preventDefault();
        $("#profile-image-selector").click();
    });
    $("#selectPictureAlt").click(function (e) {
        e.preventDefault();
        $("#profile-image-selector").click();
    });

    //CHANGING PROFILE PICTURE

    //cropping the picture


    var blobConverter = function dataURItoBlob(dataURI) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        //Old Code
        //write the ArrayBuffer to a blob, and you're done
        //var bb = new BlobBuilder();
        //bb.append(ab);
        //return bb.getBlob(mimeString);

        //New Code
        return new Blob([ab], {type: mimeString});


    };

    var selectionCount = 0;

    $('#profile-image-selector').on('change', function () {
        var profileImage = this.files[0];

        if (this.accept && $.inArray(profileImage.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }

        if ((/^image\/(png|jpeg|jpg)$/i).test(profileImage.type)) {
            var reader = new FileReader(profileImage);

            reader.readAsDataURL(profileImage);

            reader.onload = function (e) {
                var data = e.target.result;

                //selection counter
                selectionCount = selectionCount + 1;

                //show the modal 
                $("#profileImageModal").modal('show');

                //creating the image variable
                var image = $('#img-crop');
                var cropBoxData;
                var canvasData;
                var dataBlob;

                //change the image src
                image.attr('src', data).fadeIn();

                //start the cropper
                image.cropper({
                    aspectRatio: 2 / 2,
                    ready: function () {
                        image.cropper('setCanvasData', canvasData);
                        image.cropper('setCropBoxData', cropBoxData);
                    }

                });

                if (selectionCount > 1) {
                    //console.log("second selection");
                    image.cropper('replace', data);
                }


                //when the save button is pressed
                $('#save-crop-picture').on('click', function () {
                    cropBoxData = image.cropper('getCropBoxData');
                    canvasData = image.cropper('getCroppedCanvas', {
                        fillColor: '#fff',
                        imageSmoothingEnabled: false,
                        imageSmoothingQuality: 'high'
                    }).toDataURL('image/jpeg');


                    var newBlob = blobConverter(canvasData);
                    //ajax post
                    var formData = new FormData();

                    formData.append('profile_picture', newBlob);

                    //show the upload loading div
                    $('#uploading-image').show();
                    //hiding the modal
                    $("#profileImageModal").modal('hide');

                    var url = "/update_picture";

                    $.ajax({
                        type: "POST",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $("input[name='_token']").val()
                        },
                        enctype: 'multipart/form-data',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data) {


                            var response = JSON.parse(data);
                            $('#my-profile-pic').attr('src', '/images/users/' + response.image_name).fadeIn();
                            //hide the uploading overlay
                            $('#uploading-image').hide();

                            //destroy the cropper
                            image.cropper('destroy');

                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $("body").html(data.responseText);
                        }
                    });


                })


            };
        }


    });


//CREATING A TEAM
    $("#create-team-submit").click(function (e) {
        e.preventDefault();

        var url = "/team/create";
        var formData = new FormData();


        formData.append('team_name', $('#team-name').val());
        formData.append('category', $('#industry').val());
        formData.append('description', $('#team-bio').val());
        //formData.append('team_logo', file);


        console.log(formData);

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                //hide modal
                $("#createTeamModal").modal('hide');
                var response = JSON.parse(data);
                //populate the team div
                //remove the create team modal
                $('#createTeamModal').detach();

                window.location.href = "/team/edit/" + response.slug;

                //add the member of details
                //$('.about-user').append('<h6>Member of ' + response.team_name + '</h6><p><a href="#"><i class="fa fa-edit"></i> Position in team:</a></p>')

            },
            error: function (data) {
                console.log('Error:', data);
                $("body").html(data.responseText);


            }
        });


        //dropzone

        $('.upload-dropzone').on('dragover', function () {
            $(this).addClass('hover');
        });

        $('.upload-dropzone').on('dragleave', function () {
            $(this).removeClass('hover');
        });

        $('.upload-dropzone input').on('change', function (e) {
            file = this.files[0];

            $('.upload-dropzone').removeClass('hover');

            if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
                return alert('File type not allowed.');
            }

            $('.upload-dropzone').addClass('dropped');
            $('.upload-dropzone img').remove();

            if ((/^image\/(png|jpeg|jpg)$/i).test(file.type)) {
                var reader = new FileReader(file);

                reader.readAsDataURL(file);

                reader.onload = function (e) {
                    var data = e.target.result,
                        $img = $('<img />').attr('src', data).fadeIn();

                    $('.upload-dropzone div').html($img);
                };
            } else {
                var ext = file.name.split('.').pop();

                $('.upload-dropzone div').html(ext);
            }
        });

//CREATING A TEAM
        $("#create-team-submit").click(function (e) {
            e.preventDefault();

            var url = "/team/create";
            var formData = new FormData();


            formData.append('team_name', $('#team-name').val());
            formData.append('category', $('#industry').val());
            formData.append('description', $('#team-bio').val());
            formData.append('team_logo', file);


            console.log(formData);

            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    //hide modal
                    $("#createTeamModal").modal('hide');
                    var response = JSON.parse(data);
                    //populate the team div
                    $('#team').html('<p>You currently don\'t have other members in your team</p>' +
                        '<button class="btn btn-outline-primary" id="invite_team" ' +
                        'data-toggle="modal"data-target="#createTeamModal">Invite members ' +
                        '</button>')

                    //remove the create team modal
                    $('#createTeamModal').detach();

                    //add the member of details
                    $('.about-user').append('<h6>Member of ' + response.team_name + '</h6><p><a href="#"><i class="fa fa-edit"></i> Position in team:</a></p>')

                },
                error: function (data) {
                    console.log('Error:', data);

                    var response = JSON.parse(data.responseText);
                    if (response.bio) {
                        alert(response.bio)
                    } else {
                        $("body").html(data.responseText);
                    }


                }
            });


        });


    });


    //saving the basic info
    //variables

    var fullname, gender, level, department, hallOfResidence, birthday, birthYear, birthMonth, birthDate;
    fullname = $('#fullname');
    gender = $('#gender');
    level = $('#level');
    department = $('#department');
    hallOfResidence = $('#residence');
    birthday = $('input[name="birthdayForm_birthDay"]');
    birthYear = $('.birthYear');
    birthMonth = $('.birthMonth');
    birthDate = $('.birthDate');


    // checking to see if there has been a change in any of the input fields
    var basicInfoSaveBtn = $('#basic-info-save');

    $("#fullname,#gender,#department,#level,.birthDate,#residence,.birthMonth,.birthYear").on('change', function () {
        if (basicInfoSaveBtn.hasClass("disabled")) {
            basicInfoSaveBtn.removeClass("disabled");
        }
    });

    //when the basic info save button is clicked
    basicInfoSaveBtn.on('click', function (e) {
        e.preventDefault();

        // the url

        //if the button does not have a disabled class
        if (!basicInfoSaveBtn.hasClass("disabled")) {
            $('#basic-info-saving').show();

            var url = "/update_bi";
            //form data
            var formData = {
                fullname: fullname.val(),
                gender: gender.val(),
                level: level.val(),
                birthday: birthday.val(),
                department: department.val(),
                hallOfResidence: hallOfResidence.val()
            };

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#basic-info-errors').html('');

                    $('#basic-info-saving').hide();

                    //add the disabled class
                    if (!basicInfoSaveBtn.hasClass("disabled")) {
                        basicInfoSaveBtn.addClass("disabled");
                    }

                },
                error: function (data) {
                    $('#basic-info-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#basic-info-errors').html(string)
                }
            });

        }

    });


    //Extra info

    var address, phoneNumber, additionalEmail;
    address = $('#address');
    phoneNumber = $('#phone-number');
    additionalEmail = $('#additional-email');
    favouriteEntrepreneur = $('#favourite-entrepreneur');


    // checking to see if there has been a change in any of the input fields


    var extraInfoSaveBtn = $('#extra-info-save');

    $("#address,#phone-number,#additional-email,#favourite-entrepreneur").on('change', function () {
        if (extraInfoSaveBtn.hasClass("disabled")) {
            extraInfoSaveBtn.removeClass("disabled");
        }
    });

    //when the basic info save button is clicked
    extraInfoSaveBtn.on('click', function (e) {
        e.preventDefault();

        // the url

        //if the button does not have a disabled class
        if (!extraInfoSaveBtn.hasClass("disabled")) {
            $('#extra-info-saving').show();

            var url = "/update_ei";

            //form data
            var formData = {
                phone_number: phoneNumber.val(),
                additional_email: additionalEmail.val(),
                address: address.val(),
                favourite_entrepreneur: favouriteEntrepreneur.val()
            };

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {

                    $('#extra-info-errors').html('');
                    $('#extra-info-saving').hide();

                    //add the disabled class
                    if (!extraInfoSaveBtn.hasClass("disabled")) {
                        extraInfoSaveBtn.addClass("disabled");
                    }

                },
                error: function (data) {
                    $('#extra-info-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#extra-info-errors').html(string)

                }
            });

        }

    });


    //EDITING BIO
    var aboutMeSaveBtn = $("#about-me-save");
    var aboutMe = $('#about-me-text');

    aboutMe.on('change', function () {
        if (aboutMeSaveBtn.hasClass("disabled")) {
            aboutMeSaveBtn.removeClass("disabled");
        }
    });

    aboutMeSaveBtn.click(function (e) {
        e.preventDefault();


        if (!aboutMeSaveBtn.hasClass("disabled")) {
            $('#about-me-saving').show();
            var url = "/bio";

            var formData = {
                bio: aboutMe.val()
            };

            //ajax

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#bio-errors').html('');

                    $('#about-me-saving').hide();
                    if (!aboutMeSaveBtn.hasClass("disabled")) {
                        aboutMeSaveBtn.addClass("disabled");
                    }
                },
                error: function (data) {
                    $('#about-me-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#bio-errors').html(string)
                }

            });

        }


    });


    //TEAM POSITION
    var teamRoleSaveBtn = $("#team-role-save");
    var teamRole = $('#team-role-text');

    teamRole.on('change', function () {
        if (teamRoleSaveBtn.hasClass("disabled")) {
            teamRoleSaveBtn.removeClass("disabled");
        }
    });

    teamRoleSaveBtn.click(function (e) {
        e.preventDefault();


        if (!teamRoleSaveBtn.hasClass("disabled")) {
            $('#team-role-saving').show();
            var url = "/update_role";

            var formData = {
                team_role: teamRole.val()
            };

            //ajax

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#team-role-errors').html('');

                    $('#team-role-saving').hide();
                    if (!teamRoleSaveBtn.hasClass("disabled")) {
                        teamRoleSaveBtn.addClass("disabled");
                    }
                },
                error: function (data) {

                    var response = JSON.parse(data.responseText);
                    if (response.bio) {
                        alert(response.bio)
                    } else {
                        $('#team-role-saving').hide();
                        var response = JSON.parse(data.responseText);
                        var count = 0;
                        var string;
                        $.each(response, function (key, value) {
                            count = count + 1;

                            if (count === 1) {
                                string = value
                            }
                            else if (count === response.length) {
                                string += '&' + value
                            }
                            else {
                                string += ',' + value
                            }


                        });
                        $('#team-role-errors').html(string)

                    }


                }
            });

        }


    });


    //social media

    var facebook, twitter, instagram, linkedin;
    var socialSaveBtn = $('#social-save');
    facebook = $('#facebook');
    twitter = $('#twitter');
    instagram = $('#instagram');
    linkedin = $('#linkedin');

    facebook.on('change', function () {
        if (socialSaveBtn.hasClass("disabled")) {
            socialSaveBtn.removeClass("disabled")
        }
    });
    instagram.on('change', function () {
        if (socialSaveBtn.hasClass("disabled")) {
            socialSaveBtn.removeClass("disabled")
        }
    });
    twitter.on('change', function () {
        if (socialSaveBtn.hasClass("disabled")) {
            socialSaveBtn.removeClass("disabled")
        }
    });
    linkedin.on('change', function () {
        if (socialSaveBtn.hasClass("disabled")) {
            socialSaveBtn.removeClass("disabled")
        }
    });

    socialSaveBtn.on('click', function () {

        if (!socialSaveBtn.hasClass("diabled")) {
            $('#social-saving').show();
            var url = "/update_social";
            var formData = {
                facebook: facebook.val(),
                twitter: twitter.val(),
                instagram: instagram.val(),
                linkedin: linkedin.val(),
            };

            $.ajax({
                type: 'PATCH',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#social-saving').hide();
                },
                error: function (data) {
                    $('#social-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#social-errors').html(string)

                }
            })

        }
    });


    //search
    $('#search-submit').on('click', function (e) {
        e.preventDefault();

        var searchItem = $('#search').val();

        // submit the search value to the route

        $.ajax({
            type: 'POST',
            url: '/search',
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            data: {searchFor: searchItem},
            dataTye: 'json',
            success: function (data) {
                $('#search-results').html(data.html).fadeIn("1500")
            },
            error: function (data) {

                //$('body').html(data.responseText);
            }
        })

    });
    $('#searchTeamModal').on('hidden.bs.modal', function (e) {
        $('#search-results').html("");
        $('#search').val("");
    });


    //search team
    $('#search-team-submit').on('click', function (e) {
        e.preventDefault();

        var searchItem = $('#search-team').val();

        // submit the search value to the route

        $.ajax({
            type: 'POST',
            url: '/search_team',
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            data: {searchFor: searchItem},
            dataTye: 'json',
            success: function (data) {
                //console.log(data);
                $('#search-team-results').html(data.html).fadeIn("1500")
            },
            error: function (data) {
                //console.log(data.responseText);
                ////$('body').html(data.responseText);
            }
        })

    });

    $('#joinTeamModal').on('hidden.bs.modal', function (e) {
        $('#search-team-results').html("");
        $('#search-team').val("");
    });


    //remove member button on click

    $('.request').on('click', function (e) {
        e.preventDefault();

        var to = $(this).data('request_to');
        var type = $(this).data('type');

        var url = '/request/team_role';
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            data: {type: type, to: to},
            dataType: 'json',
            success: function (data) {
                console.log(data);

            },
            error: function (data) {
                console.log('Error:', data.responseText);
                //$('body').html(data.responseText)
            }
        });

    });

    $('#notification-drop-down').on('hidden.bs.dropdown', function () {
        var url = '/markasread';

        if (!parseInt($(this).data("count")) < 1) {
            $.ajax({
                type: "GET",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#notification-count').html(data.unreadNotifications);
                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data.responseText);
                   // $('body').html(data.responseText)
                }
            });
        }

    });


    $('.approve-join').on('click', function () {
        var sendersId = $(this).data("senders-id");
        var receiversId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/approve";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {senders_id: sendersId,receivers_id: receiversId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {

                $('#request' + requestId).detach();
                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }

                $('#mem_num').html(data.num_members_left);
                $('#team-members-wrapper').append(data.html);

                var num = parseInt(data.num_members);
                if (num === 5) {
                    $('#buttonForInvite').html('<a href="#" class="btn btn-primary size-2 float-right disabled" style="margin-top:2px; margin-right: 0 ">Team Full</a>');
                } else {
                    $('#buttonForInvite').html(' <a href="#" class="btn btn-primary size-2 float-right" id="search_team_members" data-toggle="modal" data-target="#searchTeamModal" style="margin-top:2px; margin-right: 0 ">Invite Member</a>');
                }

            },
            error: function (data) {
                console.log(data.responseText);
            }

        })
    });
    $('.reject-join').on('click', function () {
        var sendersId = $(this).data("senders-id");
        var receiversId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/reject";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {senders_id: sendersId,receivers_id: receiversId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {

                $('#request' + requestId).detach();
                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }

            },
            error: function (data) {
                console.log(data.responseText);
            }

        })
    });

    $('.cancel-join').on('click', function () {
        var sendersId = $(this).data("senders-id");
        var receiversId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/cancel_join";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {senders_id: sendersId, receivers_id: receiversId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {
                console.log(data);
                $('#request' + requestId).detach();

                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }


            },
            error: function (data) {
               // $('body').html(data.responseText);
                console.log(data.responseText);
            }

        })
    });

    $('.accept-invite').on('click', function () {
        var sendersId = $(this).data("senders-id");
        var receiversId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/accept_invite";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {senders_id: sendersId, receivers_id: receiversId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {
                console.log(data);
                $('#request' + requestId).detach();

                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }
                if (data.refresh === true) {
                    location.reload(true)
                }


            },
            error: function (data) {
                console.log(data.responseText);
                //$('body').html(data.responseText);
            }

        })
    });

    $('.cancel-invite').on('click', function () {
        var receiverId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/cancel_invite";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {receiver_id: receiverId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {
                console.log(data);
                $('#request' + requestId).detach();

                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }


            },
            error: function (data) {
                console.log(data.responseText);
            }

        })
    });

    $('.reject-invite').on('click', function () {
        var sendersId = $(this).data("senders-id");
        var receiversId = $(this).data("receivers-id");
        var requestId = $(this).data("request-id");
        var url = "/reject_invite";

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: {senders_id: sendersId, receivers_id: receiversId, request_id: requestId},
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            },
            success: function (data) {
                console.log(data);
                $('#request' + requestId).detach();

                if (parseInt(data.request_count) === 0) {
                    $('#requests-wrapper').detach();
                }


            },
            error: function (data) {
                console.log(data.responseText);
            }

        })
    });


});

function invite(e) {
    var inviteId = $(e).data("id");
    var url = "/invite";

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data: {member_id: inviteId},
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        },
        success: function (data) {
            $('#searchTeamModal').modal('hide');
            if (data.refresh === true) {
                location.reload(true)
            }

        },
        error: function (data) {
            console.log(data.responseText);
            //$('body').html(data.responseText);
        }

    })


}

function join(e) {
    var teamId = $(e).data("team-id");
    var url = "/join";

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data: {team_id: teamId},
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        },
        success: function (data) {

            $('#team-actions').html(' <button class="btn btn-success size-2 float-right disabled" id="pending" onclick=""' +
                '                                data-team-id="{{$result->id}}">Pending' +
                '                        </button>');


            location.reload(true);


            // $('#joinTeamModal').modal('hide');
            //$('#mem_num').html(data.num_members);
            //$('#team-members-wrapper').append(data.html);
            console.log(data);
        },
        error: function (data) {
            //$('body').html(data.responseText);
            console.log(data.responseText);

        }

    })
}

function removeMember(e) {
    // e.preventDefault();
    var memberId = $(e).data("member-id");
    var url = "/remove";

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data: {member_id: memberId},
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        },
        success: function (data) {
            $('#member' + data.member_id).detach();
            var num = parseInt(data.num_members);
            if (num < 5) {
                $('#buttonForInvite').html(' <a href="#" class="btn btn-primary size-2 float-right" id="search_team_members" data-toggle="modal" data-target="#searchTeamModal" style="margin-top:2px; margin-right: 0 ">Invite Member</a>');
            }
            $('#mem_num').html(data.num_members_left);

        },
        error: function (data) {
            console.log(data.responseText);
            //$('body').html(data.responseText);
        }

    })
}

function leaveTeam(e) {
    // e.preventDefault();
    var teamId = $(e).data("team-id");
    var url = "/leave";

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data: {team_id: teamId},
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        },
        success: function (data) {
            //refresh
            if (data.refresh === true) {
                location.reload(true)
            }

        },
        error: function (data) {
            console.log(data.responseText);
            //$('body').html(data.responseText);
        }

    })
}





