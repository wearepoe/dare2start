$(document).ready(function () {

    var teamInfoSaveBtn = $('#basic-details-save');

    var teamName, teamBio, teamIndustry, teamNumber, teamEmail, teamMission, teamVision, teamObjectives, teamWebsite,
        teamFacebook, teamTwitter, teamInstagram, teamLinkedin;

    teamName = $('#teamName');
    teamBio = $('#teamBio');
    teamIndustry = $('#teamIndustry');

    $('#teamName,#teamBio,#teamIndustry').on('change', function () {
        if (teamInfoSaveBtn.hasClass("disabled")) {
            teamInfoSaveBtn.removeClass("disabled");
        }
    });
    teamInfoSaveBtn.on('click', function (e) {
        e.preventDefault();

        // the url

        //if the button does not have a disabled class
        if (!teamInfoSaveBtn.hasClass("disabled")) {

            var url = "/team/update_bi";
            //form data
            var formData = {
                teamName: teamName.val(),
                teamBio: teamBio.val(),
                teamIndustry: teamIndustry.val(),
            };

            $('#team-basic-info-saving').show();

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    // console.log(data);
                    $('#team-basic-info-saving').hide();
                    //add the disabled class
                    if (!teamInfoSaveBtn.hasClass("disabled")) {
                        teamInfoSaveBtn.addClass("disabled");
                    }

                },
                error: function (data) {
                    console.log('Error:', data.responseText);

                    $('#team-basic-info-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#basic-details-errors').html(string)

                    //$('body').html(data.responseText)
                }
            });

        }
    });

    //Extra details
    var teamExtraInfoSave = $('#teamExtraInfoSave');

    teamNumber = $('#teamNumber');
    teamEmail = $('#teamEmail');
    teamMission = $('#teamMission');
    teamVision = $('#teamVision');
    teamObjectives = $('#teamObjectives');

    $('#teamNumber,#teamEmail,#teamMission,#teamVision,#teamObjectives').on('change', function () {
        if (teamExtraInfoSave.hasClass("disabled")) {
            teamExtraInfoSave.removeClass("disabled");
        }
    });

    teamExtraInfoSave.on('click', function (e) {
        e.preventDefault();

        // the url

        //if the button does not have a disabled class
        if (!teamExtraInfoSave.hasClass("disabled")) {

            var url = "/team/update_extra_i";
            //form data
            var formData = {
                teamNumber: teamNumber.val(),
                teamEmail: teamEmail.val(),
                teamMission: teamMission.val(),
                teamVision: teamVision.val(),
                teamObjectives: teamObjectives.val(),
            };

            $('#team-extra-info-saving').show();

            $.ajax({
                type: "PATCH",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    $('#team-extra-info-saving').hide();

                    //add the disabled class
                    if (!teamExtraInfoSave.hasClass("disabled")) {
                        teamExtraInfoSave.addClass("disabled");
                    }

                },
                error: function (data) {
                    //console.log('Error:', data.responseText);
                    $('body').html(data.responseText)
                    $('#team-extra-info-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#extra-info-errors').html(string)

                }
            });

        }
    });


    //social
    var teamLinkSaveBtn = $('#teamLinkSaveBtn');
    teamWebsite = $('#teamWebsite');
    teamFacebook = $('#teamFacebook');
    teamTwitter = $('#teamTwitter');
    teamInstagram = $('#teamInstagram');
    teamLinkedin = $('#teamLinkedin');

    $('#teamWebsite,#teamFacebook,#teamTwitter,#teamInstagram,#teamLinkedin').on('change', function () {
        if (teamLinkSaveBtn.hasClass("disabled")) {
            teamLinkSaveBtn.removeClass("disabled");
        }
    });

    teamLinkSaveBtn.on('click', function () {

        if (!teamLinkSaveBtn.hasClass("diabled")) {
            var url = "/team/update_social";
            var formData = {
                website: teamWebsite.val(),
                facebook: teamFacebook.val(),
                twitter: teamTwitter.val(),
                instagram: teamInstagram.val(),
                linkedin: teamLinkedin.val()
            };
            console.log(formData);

            $('#social-saving').show();

            $.ajax({
                type: 'PATCH',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("input[name='_token']").val()
                },
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#social-saving').hide();
                },
                error: function (data) {
                    //console.log(data.responseText)
                    $('#social-saving').hide();
                    var response = JSON.parse(data.responseText);

                    var count = 0;
                    var string;
                    $.each(response, function (key, value) {
                        count = count + 1;

                        if (count === 1) {
                            string = value
                        }
                        else if (count === response.length) {
                            string += '&' + value
                        }
                        else {
                            string += ',' + value
                        }


                    });
                    $('#social-errors').html(string)


                }
            })

        }
    });


    //team logo upload
    $('#uploading-image').hide();

    //global file variable
    var file;


    $(".selectPicture").click(function (e) {
        e.preventDefault();
        $("#team-logo-selector").click();
    });

    //CHANGING PROFILE PICTURE

    //cropping the picture

    var selectionCount = 0;

    var blobConverter = function dataURItoBlob(dataURI) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        //Old Code
        //write the ArrayBuffer to a blob, and you're done
        //var bb = new BlobBuilder();
        //bb.append(ab);
        //return bb.getBlob(mimeString);

        //New Code
        return new Blob([ab], {type: mimeString});


    }

    $('#team-logo-selector').on('change', function () {
        var profileImage = this.files[0];


        if (this.accept && $.inArray(profileImage.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }


        if ((/^image\/(png|jpeg|jpg)$/i).test(profileImage.type)) {
            var reader = new FileReader(profileImage);


            reader.readAsDataURL(profileImage);

            reader.onload = function (e) {

                //selection counter
                selectionCount = selectionCount + 1;

                var data = e.target.result;
                console.log("first selection");
                //show the modal
                $("#teamLogoModal").modal('show');


                //creating the image variable
                var image = $('#img-crop');
                var cropBoxData;
                var canvasData;
                var dataBlob;

                //change the image src
                image.attr('src', data).fadeIn();

                //start the cropper

                image.cropper({
                    aspectRatio: 2 / 2,
                    ready: function () {
                        image.cropper('setCanvasData', canvasData);
                        image.cropper('setCropBoxData', cropBoxData);
                    }

                });

                if (selectionCount > 1) {
                    //console.log("second selection");
                    image.cropper('replace', data);
                }

                //when the save button is pressed
                $('#save-crop-picture').on('click', function () {
                    cropBoxData = image.cropper('getCropBoxData');
                    canvasData = image.cropper('getCroppedCanvas', {
                        fillColor: '#fff',
                        imageSmoothingEnabled: false,
                        imageSmoothingQuality: 'high'
                    }).toDataURL('image/jpeg');

                    var newBlob = blobConverter(canvasData);


                    //ajax post
                    var formData = new FormData();

                    formData.append('team_logo', newBlob);

                    //show the upload loading div
                    $('#uploading-image').show();
                    $("#teamLogoModal").modal('hide');


                    var url = "/teamlogo";

                    $.ajax({
                        type: "POST",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $("input[name='_token']").val()
                        },
                        enctype: 'multipart/form-data',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data) {

                            var response = JSON.parse(data);
                            $('#my-team-logo').attr('src', '/images/teamlogos/' + response.image_name).fadeIn();
                            //hide the uploading overlay
                            $('#uploading-image').hide();

                            //destroy the cropper
                            //image.cropper('destroy');

                        },
                        error: function (data) {
                            console.log('Error:', data);
                            //$("body").html(data.responseText);
                        }
                    });


                })


            };
        }


    });


});