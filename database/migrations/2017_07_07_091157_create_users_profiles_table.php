<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfilesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'users_profiles', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'user_id' );
			$table->integer( 'team_id' )->default( 0 );
			$table->string( 'fullname' );
			$table->string( 'gender' )->default( "" );
			$table->string( 'email' )->unique();
			$table->string( 'image' )->default( "default.jpg" );
			$table->string( 'bio', 400 )->default( "" );
			$table->integer( 'level' )->default();
			$table->string( 'department' )->default( "" );
			$table->string( 'hall' )->default( "" );

			//extra info
			$table->string( 'date_of_birth' )->default( "" );
			$table->string( 'phone_number' )->default( "" );
			$table->string( 'additional_email' )->default( "" );
			$table->string( 'address' )->default( "" );
			$table->string( 'favourite_entrepreneur' )->default( "" );

			//social media
			$table->string( 'facebook' )->default( "" );
			$table->string( 'twitter' )->default( "" );
			$table->string( 'instagram' )->default( "" );
			$table->string( 'linkedin' )->default( "" );
			$table->string( 'team_role' )->default( "" );
			$table->string( 'role' )->default( "" );
			$table->string( 'request_status' )->default( "" );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'users_profiles' );
	}
}
