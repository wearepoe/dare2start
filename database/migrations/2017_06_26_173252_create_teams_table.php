<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'teams', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name' );
			$table->string( 'slug' );
			$table->string( 'category' );
			$table->text( 'bio' );
			$table->integer( 'user_id' );
			$table->integer( 'users_profile_id' );
			$table->string( 'image' )->default( "" );
			$table->integer( 'num_members' )->default();
			$table->string( 'status' )->default( "available" );
			//others
			$table->text( 'mission');
			$table->text( 'vision');
			$table->text( 'objectives' );
			$table->string( 'phone_number' );
			$table->string( 'email' );

			//social media
			$table->string( 'facebook' );
			$table->string( 'twitter' );
			$table->string( 'instagram' );
			$table->string( 'linkedin' );
			$table->string( 'website' );

			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'teams' );
	}
}
